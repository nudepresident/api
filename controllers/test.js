/*
 * libraries 
 */
const async = require('async');
const colors = require('colors');
const CandidateModel = require('../models/candidate');
const CategoryModel = require('../models/category');
const TagModel = require('../models/tag');
const QuestionModel = require('../models/question');
const PromiseModel = require('../models/promise');
const TestModel = require('../models/test');
const cache = require('memory-cache');


/*
 * class declaration
 */


function Test() {}


/*
 * routing
 */

var test = new Test();

module.exports.route = function(app) {
  app.get('/', test.index);
  app.get('/test', test.test);

  app.get('/v1/data', test.data);
};


/*
 * controller functions
 */


Test.prototype.index = function(req, res, next) {
  res.send({
    result: true,
    data: "hello world"
  });
};




Test.prototype.test = function(req, res, next) {
  res.send({
    result: true,
    data: "hello test"
  });
};




Test.prototype.data = function(req, res, next) {

  var version = Number.isInteger(req.params.v) ? req.params.v : global.version;

  // Cache
  var cache_key = `data_v${version}`;
  var data = cache.get(cache_key);
  if (data) {
    console.log('cached :' + cache_key);
    data.cached = true;
    res.charSet('utf-8');
    res.send(data);
    return;
  }


  var candidateModel = new CandidateModel(version);
  var categoryModel = new CategoryModel(version);
  var tagModel = new TagModel(version);
  var questionModel = new QuestionModel(version);
  var promiseModel = new PromiseModel(version);
  var testModel = new TestModel(version);

  console.info(colors.cyan('Take ALL data'));

  async.series({
    tests: function(callback) {
      testModel.tests({}, function(error, result) {
        callback(error, result);
      });
    },
    test_schedule: function(callback) {
      testModel.schedule({}, function(error, schedule) {
        if (error) return callback(error, null);
        else 
          async.series({
            current: function(cb) {
              testModel.tests({id: schedule.current}, function(err, result) {
                cb(err, result[0]);
              });
            },
            next: function(cb) {
              testModel.tests({id: schedule.next}, function(err, result) {
                cb(err, result[0]);
              });
            }
          }, function(err, result){
            callback(err, result);
          });
      });
    },
    candidates: function(callback) {
      console.info(colors.cyan('Take ALL data : CANDIDATES'));
      candidateModel.candidates({excludes:[1,4,6]}, function(error, result){
        callback(error, result);
      });
    },
    categories: function(callback) {
      console.info(colors.cyan('Take ALL data : CATEGORIES'));
      categoryModel.categories({}, function(error, result){
        delete result.info;
        for (var i in result) {
          var r = result[i];
          r.issues = r.issues.split(',');
        }
        callback(error, result);
      });
    },
    tags: function(callback) {
      console.info(colors.cyan('Take ALL data : TAGS'));
      tagModel.tags({}, function(error, result){
        callback(error, result);
      });
    },
    common_questions: function(callback) {
      console.info(colors.cyan('Take ALL data : COMMON QUESTIONS'));
      questionModel.questions({common:1}, function(error, result){
        delete result.info;
        for (var i in result) {
          var r = result[i];
          r.answers = r.answers.split(',');

          r.answers_by_candidate = {
            // '1': r.c_answer_1,
            '2': r.c_answer_2,
            '3': r.c_answer_3,
            // '4': r.c_answer_4,
            '5': r.c_answer_5,
            // '6': r.c_answer_6,
            '7': r.c_answer_7,
            '8': r.c_answer_8,
          }

          // delete r.c_answer_1;
          delete r.c_answer_2;
          delete r.c_answer_3;
          // delete r.c_answer_4;
          delete r.c_answer_5;
          // delete r.c_answer_6;
          delete r.c_answer_7;
          delete r.c_answer_8;
        }
        callback(error, result);
      });
    },
    questions_by_category: function(callback) {
      console.info(colors.cyan('Take ALL data : QUESTIONS BY CATEGORY'));
      questionModel.questions({common:0}, function(error, result){
        delete result.info;

        var bc = {};

        for (var i in result) {
          var r = result[i];
          r.answers = r.answers.split(',');

          r.answers_by_candidate = {
            // '1': r.c_answer_1,
            '2': r.c_answer_2,
            '3': r.c_answer_3,
            // '4': r.c_answer_4,
            '5': r.c_answer_5,
            // '6': r.c_answer_6,
            '7': r.c_answer_7,
            '8': r.c_answer_8,
          }

          // delete r.c_answer_1;
          delete r.c_answer_2;
          delete r.c_answer_3;
          // delete r.c_answer_4;
          delete r.c_answer_5;
          // delete r.c_answer_6;
          delete r.c_answer_7;
          delete r.c_answer_8;

          if (!bc[r.category_id]) {
            bc[r.category_id] = [];
          }

          bc[r.category_id].push(r);

        }


        callback(error, bc);
      });
    },
    promise_by_tag: function(callback) {
      console.info(colors.cyan('Take ALL data : PROMISE BY TAG'));
      promiseModel.promises({order:{by:'tag_id',asc:true}, excludes:[1,4,6]}, function(error, result){
        delete result.info;

        var bc = {};

        for (var i in result) {
          var r = result[i];

          if (!bc[r.tag_id]) {
            bc[r.tag_id] = [];
          }

          bc[r.tag_id].push(r);

        }
        callback(error, bc);
      });
    }
  }, function(error, result){
    console.info(colors.cyan('Take ALL data : RESULT'));
    if (error) {
      res.send(500, {description: 'Server error', error: error});
    } else {
      result.version = version;

      // cache
      cache.put(cache_key, result);
      res.charSet('utf-8');
      res.send(result);
    }
  });

};