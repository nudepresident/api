/*
 * libraries 
 */
const TagModel = require('../models/tag');
const UserModel = require('../models/user');
const required = require('../lib/required_helper');
const async = require('async');
// const FileHelper = require('../lib/file_helper');
const Session = require('../lib/session');
var colors = require('colors');

const cache = require('memory-cache');
const _ = require('lodash');
const fs = require('fs');
const Canvas = require('canvas');
const Image = Canvas.Image;
const Hashids = require('hashids');
const hashid = new Hashids();

const imageLib = require('../lib/image');
const ResultLib = require('../lib/result');
const FileHelper = require('../lib/file_helper');

const FROM_MASTER = true;

/*
 * class declaration
 */


function User() {}


/*
 * routing
 */

var user = new User();

module.exports.route = function(app) {
  app.get('/v1/users', user.getUsers);
  // app.post('/v1/users', user.createUser);
  app.get('/v1/users/total', user.getTotalCount);
  app.get('/v1/users/:id', user.getUser);
  app.put('/v1/users/:id', user.setUser);

  app.post('/v1/users/:id/categories', user.createUserCategories);
  app.post('/v1/users/:id/answers', user.createUserAnswer);
  app.post('/v1/users/:id/tags', user.createUserTags);
  app.post('/v1/users/:id/promise', user.createUserPromise);
  app.post('/v1/users/:id/message', user.createUserMessage);
  app.post('/v1/users/:id/candidate', user.setCandidate);
  app.post('/v1/users/:id/final_reason', user.setFinalReason);
  app.post('/v1/users/:id/favorite_promise', user.setFavoritePromise);
  app.post('/v1/users/:id/dislike_promise', user.setDislikePromise);

  app.get('/v1/users/:id/result', user.getUserResult);
  app.get('/v1/users/:id/result_image', user.getUserResultImage);
  app.get('/v1/users/:id/reset_data', user.resetUserData);
  app.get('/v1/users/:id/matching_points', user.getMatchingPoints);
  app.get('/v1/users/:id/stats', user.getStats);

  app.get('/v1/users/:id/personalized_tags', user.getTags);
  app.get('/v1/users/:id/tests', user.getTests);
  app.get('/v1/users/:id/data', user.getData);

};


/*
 * controller functions
 */


/*
 * GET users  Collection of users
 * @param   {nubmer}  offset
 * @param   {nubmer}  limit
 */
User.prototype.getUsers = function(req, res, next) {

  if (['ADMIN'].indexOf(Session.permission(req)) < 0) {
    return res.send(401, {error: "Not permitted"});
  }


  var userModel = new UserModel(global.version);

  var params = {
    offset : req.params.offset,
    limit : req.params.limit,
    status : req.params.status
  }

  async.waterfall([
    function(callback) {
      userModel.users(params, function(error, users){
        if (users) {
          callback(null, users);
        } else {
          callback({error: error}, null);
        }
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(500, error);
    }
  });
  return next();
};


/*
 * POST users  Create a user
 * @param  {string}  name
 * @param  {file}    profileimage
 */
/*
User.prototype.createUser = function(req, res, next) {


  var userModel = new UserModel(global.version);

  var user = {
    name: req.params.name
  };

  async.waterfall([
    function checkFile(callback) {
      if (req.files && req.files.profileimage) {
        var fileHelper = new FileHelper(global.version);
        fileHelper.savePhoto(req.files.profileimage, function(error, info){
          callback(error, info);
        });
      } else {
        callback(null, {}) ;
      }
    },
    function create(fileInfo, callback) {
      if (fileInfo.original) {
        user.profileimage = fileInfo.original.Location;
        user.profileimage_raw = JSON.stringify(fileInfo.original);
      }

      userModel.create({user: user}, function(error, user_id) {
        callback(error, user_id);
      });    
    },
    function get(user_id, callback) {
      userModel.user({id: user_id}, function(error, user){
        callback(error, user);
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(500, error);
    }
  });
};
*/

/*
 * GET users/:id  User object
 *
 */
User.prototype.getUser = function(req, res, next) {

  if (['ADMIN', 'LOGGED_IN'].indexOf(Session.permission(req)) < 0) {
    return res.send(401, {error: "Not permitted"});
  }

  var user = Session.user(req);
  if (!user) {
    res.send(401, {error: "Unauthorized"});
    return;
  }

  var simple = req.params.id != user.id && Session.permission(req) != 'ADMIN';

  var userModel = new UserModel(global.version);

  var params = {id: req.params.id, simple: simple};

  async.waterfall([
    function(callback) {
      userModel.user(params, function(error, user){
        if (user) {
          callback(null, user);
        } else {
          callback({error: error}, null);
        }
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(500, error);
    }
  });
  return next();
};


/*
 * POST  users/:id  Edit user
 * @params {string}  name
 * @params {number}  age
 * @params {string}  gender
 * @params {number}  location_id
 * @params {string}  job
 * @params {boolean} married - 0,1 
 * @params {boolean} has_children - 0,1
 * @param  {number}  withdraw_request - 0,1
 */
User.prototype.setUser = function(req, res, next) {

  console.info(colors.cyan('Set User'));

  // Check permission
  if ('production' == process.env.NODE_ENV && 'production' == process.env.NODE_ENV && req.user.id != req.params.id)
    return res.send(403, {code:403, description:"forbidden"});

  var userModel = new UserModel(global.version);

  var user = {
    id: req.params.id
  };

  if (req.params.name !== undefined)
      user.name = req.params.name;

  if (req.params.age !== undefined)
      user.age = req.params.age;

  if (req.params.gender !== undefined)
      user.gender = req.params.gender;

  if (req.params.location_id !== undefined)
      user.location_id = req.params.location_id;

  if (req.params.job !== undefined)
      user.job = req.params.job;

  if (req.params.married !== undefined)
      user.married = req.params.married;

  if (req.params.has_children !== undefined)
      user.has_children = req.params.has_children;


  async.waterfall([
    function update(callback) {
      console.info(colors.cyan('Set User : UPDATE'));

      userModel.update({user: user}, function(error, info){
        if (error) {
          callback({code:500, description:"Update failed", error: error}, null);
        } else {
          callback(null, info);
        }
      });
    },
    function get(info, callback) {
      console.info(colors.cyan('Set User : GET'));

      userModel.user({id: user.id}, function(error, user){
        if (error) {
          callback({code:500, description:"Database Error", error: error}, null);
        } else {
          if (!user) {
            callback({code:404, description:"Not found", error: error}, null);
          } else {
            var User = require('../object/user');
            user = new User(global.version).jsonFromRecord(user);
            callback(null, user);
          }
        }
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
  return next();
};





/*
 * POST users/:id/answer  Create a user answer
 * @params {number} question_id
 * @params {number} answer_idx
 * @params {string} answer_text
 * @params {number} matching_point_0 - 후보 ID 0 의 매칭 점수 
 * @params {number} matching_point_1 - 후보 ID 1 의 매칭 점수 
 * @params {number} matching_point_2 - 후보 ID 2 의 매칭 점수 
 * @params {number} matching_point_3 - 후보 ID 3 의 매칭 점수 
 * @params {number} matching_point_4 - 후보 ID 4 의 매칭 점수 
 * @params {number} matching_point_5 - 후보 ID 5 의 매칭 점수
 */

User.prototype.createUserAnswer = function(req, res, next) {

  console.info(colors.cyan('Create User Answer'));

  const AnswerModel = require('../models/answer');
  const QuestionModel = require('../models/question');
  var answerModel = new AnswerModel(global.version);
  var questionModel = new QuestionModel(global.version);

  async.waterfall([

    function validate(callback) {
      console.info(colors.cyan('Create User Answer : VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && 'production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});

      // Validate data
      required.all([
        'question_id',
        'answer_idx'
        ], req.params, callback);
    },

    function question(params, callback) {
      console.info(colors.cyan('Create User Answer : CALCULATE POINTS'));

      questionModel.questions({id:params.question_id, formatted: true}, function(error, questions){
        if (error){
          callback({code:500, description:"Database Error ",error:error}, null);
        } else {
          if (questions.length < 1)
            callback({code:404, description:'Question not found with question_id: '+params.question_id});
          else {
            // Calculate Points
            var q = questions[0];
            var answers = q.answers && q.answers.length > 0 ? q.answers.split(',') : [];
            var ua = answers[params.answer_idx];

            console.info(colors.red(ua), 'USER ANSWER');
            console.info(colors.red(q.answers_by_candidate['2']), 'CANDIDATE ANSWER 2');
            console.info(colors.red(q.answers_by_candidate['3']), 'CANDIDATE ANSWER 3');
            console.info(colors.red(q.answers_by_candidate['5']), 'CANDIDATE ANSWER 5');
            console.info(colors.red(q.answers_by_candidate['7']), 'CANDIDATE ANSWER 7');
            console.info(colors.red(q.answers_by_candidate['8']), 'CANDIDATE ANSWER 8');

            params.matching_point_2 = (ua == q.answers_by_candidate['2'] ? 1 : 0);
            params.matching_point_3 = (ua == q.answers_by_candidate['3'] ? 1 : 0);
            params.matching_point_5 = (ua == q.answers_by_candidate['5'] ? 1 : 0);
            params.matching_point_7 = (ua == q.answers_by_candidate['7'] ? 1 : 0);
            params.matching_point_8 = (ua == q.answers_by_candidate['8'] ? 1 : 0);
            

            callback(null, q, params);
          }
        }
      });
    },

    function checkUserCategory(question, params, callback) {
      console.info(colors.cyan('Create User Answer : CHECK VALID QUESTION'));

      // 공통 질문일 경우 SKIP
      if (question.common === '1') 
        return callback(null, question, params);

      const CategoryModel = require('../models/category');
      var categoryModel = new CategoryModel(global.version);
      categoryModel.userCategories({user_id: params.id, question_id:params.question_id}, function(error, categories){
        if (error){
          callback({code:500, description:"Database Error ",error:error}, null);
        } else {
          if (categories.length < 1)
            callback({code:400, description:'The question is not included in the category that user selected.'})
          else
            callback(null, question, params);
        }
      });
    },


    function checkExists(question, params, callback) {
      console.info(colors.cyan('Create User Answer : CHECK EXISTS'));

      answerModel.answer({user_id: params.id, question_id: params.question_id}, function(error, answer) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(answer, 'Answer');
          callback(null, question, answer, params);
        }
      });
    },

    function createOrUpdate(question, existingAnswer, params, callback) {
      console.info(colors.cyan('Create User Answer : CREATE OR UPDATE'));

      var answer = {
        user_id: params.id,
        question_id: params.question_id,
        answer_idx: params.answer_idx,
        matching_point_2: params.matching_point_2,
        matching_point_3: params.matching_point_3,
        matching_point_5: params.matching_point_5,
        matching_point_7: params.matching_point_7,
        matching_point_8: params.matching_point_8
      }

      if (params.answer_text)
        answer.answer_text = params.answer_text;

      if (existingAnswer == undefined) {
        console.info(colors.cyan('Create User Answer : CREATE'));

        answerModel.create({answer: answer}, function(error, answer_id){
          if (error) {
            callback({code: 500, description: 'Database error', error: error}, null);
          } else {
            callback(null, question, answer_id);
          }
        });
      } else {
        console.info(colors.cyan('Create User Answer : UPDATE'));

        answerModel.update({answer: answer}, function(error, info){
          if (error) {
            callback({code: 500, description: 'Database error', error: error}, null);
          } else {
            callback(null, question, existingAnswer.id);
          }
        });
      }
    },
    function fetch(question, answer_id, callback) {
      console.info(colors.cyan('Create User Answer : FETCH'));

      answerModel.answer({id: answer_id, simple: true}, function(error, answer){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, question, answer);
        }
      }, FROM_MASTER);
    },
    function format(question, answer, callback) {
      answer.question = question;
      callback(null, answer);
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};






/*
 * POST users/:id/categories  Create a user categories
 * @params {number} category_ids
 */

User.prototype.createUserCategories = function(req, res, next) {

  console.info(colors.cyan('Create User Categories'));

  // Validation 

  const CategoryModel = require('../models/category');
  var categoryModel = new CategoryModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Create User Categories : VALIDATE PARAMETERS'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});

      required.all([
        'category_ids',
        ], req.params, callback);
    },

    function validateEmpty(params, callback) {
      console.info(colors.cyan('Create User Categories : VALIDATE PARAMETERS EMPTY'));

      var ids = req.params.category_ids;
      ids = ids.replace(/s/gi, '');
      var categories = ids.length > 0 ? ids.split(',') : [];
      console.log(categories);
      if (categories.length < 1) {
        callback({code:400, description:'category_ids must not be empty'}, null);
      } else {
        callback(null, categories);
      }
    },

    function removeExistCategories(categories, callback) {
      console.info(colors.cyan('Create User Categories : REMOVE EXISTS'));

      categoryModel.deleteUserCategory({user_id: req.params.id}, function(error, info){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, categories);
        }
      });
    },

    function create(categories, callback) {
      console.info(colors.cyan('Create User Categories : CREATE'));

      async.eachSeries(categories, function(item, callback_){
        var userCat = {
          category_id: item,
          user_id: req.params.id
        }
        categoryModel.create({category: userCat}, function(error, info){
          if (error) {
            callback_({code: 500, description: 'Database error', error: error});
          } else {
            callback_();
          }
        });
      }, function(err){
        callback(err, categories);
      });
    },

    function fetch(categories, callback) {
      console.log(categories);
      console.info(colors.cyan('Create User Categories : FETCH'));

      categoryModel.categories({inIDs: categories}, function(error, categories){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, categories);
        }
      }, FROM_MASTER);
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};




/*
 * POST users/:id/tags  Create a user tags
 * @params {number} tag_ids
 */

User.prototype.createUserTags = function(req, res, next) {

  console.info(colors.cyan('Create User Tags'));

  // Validation 

  const TagModel = require('../models/tag');
  var tagModel = new TagModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Create User Tags : VALIDATE PARAMETERS'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});

      required.all([
        'tag_ids',
        ], req.params, callback);
    },

    function validateEmpty(params, callback) {
      console.info(colors.cyan('Create User Tags : VALIDATE PARAMETERS EMPTY'));

      var ids = req.params.tag_ids;
      ids = ids.replace(/s/gi, '');
      var tags = ids.length > 0 ? ids.split(',') : [];
      console.log(tags);
      if (tags.length < 1) {
        callback({code:400, description:'tag_ids must not be empty'}, null);
      } else {
        callback(null, tags);
      }
    },

    function removeExist(tags, callback) {
      console.info(colors.cyan('Create User Tags : REMOVE EXISTS'));

      tagModel.deleteUserTag({user_id: req.params.id}, function(error, info){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, tags);
        }
      });
    },

    function create(tags, callback) {
      console.info(colors.cyan('Create User Tags : CREATE'));

      async.eachSeries(tags, function(item, callback_){
        var tag = {
          tag_id: item,
          user_id: req.params.id
        }
        tagModel.createUserTag({tag: tag}, function(error, info){
          if (error) {
            callback_({code: 500, description: 'Database error', error: error});
          } else {
            callback_();
          }
        });
      }, function(err){
        callback(err, tags);
      });
    },

    function fetch(tags, callback) {
      console.log(tags);
      console.info(colors.cyan('Create User Tags : FETCH'));

      tagModel.tags({user_id: req.params.id}, function(error, tags){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, tags);
        }
      }, FROM_MASTER);
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};





/*
 * POST users/:id/promise  Create a user promise
 * @params {number} promise_id
 */

User.prototype.createUserPromise = function(req, res, next) {

  console.info(colors.cyan('Create User Promise'));

  // Validation 

  const PromiseModel = require('../models/promise');
  var promiseModel = new PromiseModel(global.version);

  const TagModel = require('../models/tag');
  var tagModel = new TagModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Create User Promise : VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});

      required.all([
        'promise_id',
        ], req.params, callback);
    },

    function getPromise(params, callback) {
      console.info(colors.cyan('Create User Promise : GET PROMISE'));

      promiseModel.promise({id: params.promise_id}, function(error, promise) {
        if (error)
          callback({code:500, description:'Database Error', error: error}, null);
        else
          callback(null, promise, params);
      });
    },

    function checkUserTag(promise, params, callback) {
      console.info(colors.cyan('Create User Promise : CHECK VALID PROMISE'));

      tagModel.userTags({user_id: params.id}, function(error, tags){
        if (error){
          callback({code:500, description:"Database Error ",error:error}, null);
        } else {
          var valid = false;
          for (var i in tags) {
            var t = tags[i];
            if (t.tag_id == promise.tag_id) {
              valid = true;
              break;
            }
          }
          if (!valid)
            callback({code:400, description:'The promise is not included in the tag that user selected.'})
          else
            callback(null, promise, params);
        }
      });
    },

    function setPromise(promise, info, callback) {
      console.info(colors.cyan('Create User Promise : SET PROMISE'));

      // Set promise on user tag
      var user_tag = {
        user_id: req.params.id,
        tag_id: promise.tag_id,
        selected_promise: promise.id,
        // matching_point_1: (promise.candidate_id == 1 ? 1 : 0),
        matching_point_2: (promise.candidate_id == 2 ? 1 : 0),
        matching_point_3: (promise.candidate_id == 3 ? 1 : 0),
        // matching_point_4: (promise.candidate_id == 4 ? 1 : 0),
        matching_point_5: (promise.candidate_id == 5 ? 1 : 0),
        // matching_point_6: (promise.candidate_id == 6 ? 1 : 0)
        matching_point_7: (promise.candidate_id == 7 ? 1 : 0),
        matching_point_8: (promise.candidate_id == 8 ? 1 : 0),
      };

      tagModel.updateUserTag({user_tag: user_tag}, function(error, info){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          callback(null, promise.id);
        }
      });

    },
    function fetch(promise_id, callback) {
      console.info(colors.cyan('Create User Promise : FETCH'));

      promiseModel.userPromises({promise_id: promise_id}, function(error, promises){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          if (promises[0])
            callback(null, promises[0]);
          else
            callback({code: 500, description: 'Failed adding promise', error: error}, null)
        }
      }, FROM_MASTER);
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/*
 * POST users/:id/message  Create a user message
 * @params {number} promise_id
 */
User.prototype.createUserMessage = function(req, res, next) {
  console.info(colors.cyan('Create User Message'));


  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Create User Message: VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});


      required.all([
        //'message',
        'final_reason'
        ], req.params, callback);
    },
    function update(params, callback) {
      console.info(colors.cyan('Create User Message: UPDATE'));
      
      var test = {
        user_id: params.id,
        message: params.message || null,
        final_reason: params.final_reason
      };

      if (params.favorite_promise != undefined)
        test.favorite_promise = params.favorite_promise;

      if (params.dislike_promise != undefined)
        test.dislike_promise = params.dislike_promise;

      if (params.final_reason != undefined) {
        if (Array.isArray(params.final_reason))
          test.final_reason = params.final_reason.join(',');
        else
          test.final_reason = params.final_reason;
      }

      userModel.updateTest({test: test, sent_message: true}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, info);
        }
      });

    },
    function get(info, callback) {
      console.info(colors.cyan('Create User Message: GET'));

      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          user = {
            favorite_promise: user.favorite_promise,
            dislike_promise: user.dislike_promise,
            message: user.message,
            final_reason: user.final_reason
          }
          callback(null, user);
        }
      }, FROM_MASTER);
    }

  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};


/*
 * POST users/:id/final_reason  Save final reason
 * @params {string} final_reason
 */
User.prototype.setFinalReason = function(req, res, next) {
  console.info(colors.cyan('Save final reason'));


  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Save final reason: VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});


      required.all([
        //'message',
        'final_reason'
        ], req.params, callback);
    },
    function update(params, callback) {
      console.info(colors.cyan('Save final reason: UPDATE'));
      
      var test = {
        user_id: params.id,
        final_reason: params.final_reason,
      };

      if (params.final_reason != undefined) {
        if (Array.isArray(params.final_reason))
          test.final_reason = params.final_reason.join(',');
        else
          test.final_reason = params.final_reason;
      }

      userModel.updateTest({test: test}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, info);
        }
      });

    },
    function get(info, callback) {
      console.info(colors.cyan('Save final reason: GET'));

      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          user = {
            final_reason: user.final_reason
          }
          callback(null, user);
        }
      }, FROM_MASTER);
    },

  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};


/*
 * POST users/:id/favorite_promise  Save final reason
 * @params {string} favorite_promise
 */
User.prototype.setFavoritePromise = function(req, res, next) {
  console.info(colors.cyan('Save favorite_promise'));


  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Save favorite_promise: VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});


      required.all([
        'favorite_promise'
        ], req.params, callback);
    },
    function update(params, callback) {
      console.info(colors.cyan('Save favorite_promise: UPDATE'));
      
      var test = {
        user_id: params.id,
        favorite_promise: params.favorite_promise,
      };

      userModel.updateTest({test: test}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, info);
        }
      });

    },
    function get(info, callback) {
      console.info(colors.cyan('Save favorite_promise: GET'));

      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          user = {
            favorite_promise: user.favorite_promise
          }
          callback(null, user);
        }
      }, FROM_MASTER);
    },

  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};


/*
 * POST users/:id/dislike_promise  Save final reason
 * @params {string} dislike_promise
 */
User.prototype.setDislikePromise = function(req, res, next) {
  console.info(colors.cyan('Save dislike_promise'));


  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Save dislike_promise: VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});


      required.all([
        'dislike_promise'
        ], req.params, callback);
    },
    function update(params, callback) {
      console.info(colors.cyan('Save dislike_promise: UPDATE'));
      
      var test = {
        user_id: params.id,
        dislike_promise: params.dislike_promise,
      };

      userModel.updateTest({test: test}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, info);
        }
      });

    },
    function get(info, callback) {
      console.info(colors.cyan('Save dislike_promise: GET'));

      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          user = {
            dislike_promise: user.dislike_promise
          }
          callback(null, user);
        }
      }, FROM_MASTER);
    },

  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/*
 * POST users/:id/candidate  Set user candidate
 * @params {number*} candidate_id
 * @params {string*} type - current, final
 */
User.prototype.setCandidate = function(req, res, next) {
  console.info(colors.cyan('Set User Candidate'));

  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Set User Candidate : VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});


      required.all([
        'type',
        ], req.params, callback);
    },
    function validateType(params, callback) {
      console.info(colors.cyan('Set User Candidate : VALIDATE TYPE'));

      if (['current','final'].indexOf(params.type) != -1)
        callback(null, params);
      else
        callback({'code':400, description:'invalid value of type'}, null);
    },
    function update(params, callback) {
      console.info(colors.cyan('Set User Candidate : UPDATE'));

      var type = params.type;
      var test = { user_id: params.id };

      if (params.candidate_id == 'null')
        params.candidate_id = null;

      if (type == 'current')
        test.current_candidate = params.candidate_id;
      else
        test.final_candidate = params.candidate_id;


      userModel.updateTest({test: test}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, params);
        }
      });

    },
    function get(params, callback) {
      console.info(colors.cyan('Set User Candidate : GET'));

      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, description: 'Database error', error: error}, null);
        } else {
          var u = {};
          if (params.type == 'current')
            u.current_candidate = user.current_candidate;
          else
            u.final_candidate = user.final_candidate;
          callback(null, u);
        }
      }, FROM_MASTER);
    }

  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};




/*
 * GET users/:id/matching_points
 *
 */

User.prototype.getMatchingPoints = function(req, res, next) {

  var User = require('../object/user');

  new User(global.version).matchingPoints(req.params.id, function(error, result){
    if (error) {
      console.log(error);
      return res.send(error.code, error);
    }
    
    res.send(result);
  });

};


/*
 * GET users/:id/result - Get user result
 *
 */

User.prototype.getUserResult = function(req, res, next) {

  var version = Number.isInteger(parseInt(req.params.version)) ? req.params.version : global.version;

  const CandidateModel = require('../models/candidate');
  const CategoryModel = require('../models/category');
  const TagModel = require('../models/tag');
  const QuestionModel = require('../models/question');
  const PromiseModel = require('../models/promise');
  const ResultModel = require('../models/result');

  var candidateModel = new CandidateModel(version);
  var categoryModel = new CategoryModel(version);
  var tagModel = new TagModel(version);
  var questionModel = new QuestionModel(version);
  var promiseModel = new PromiseModel(version);
  var resultModel = new ResultModel(version);

  console.log('global verion :' + version);

  console.info(colors.cyan('Get User Result ' + version ));

  async.series({
    candidates: function(callback) {
      console.info(colors.cyan('Get User Result : CANDIDATES'));
      candidateModel.candidates({excludes:[1,4,6]}, function(error, result){
        callback(error, result);
      });
    },
    total: function(callback) {
      console.info(colors.cyan('Get User Result : TOTAL'));
      new ResultLib(version).getTotalResult(req.params.id, callback);
    },
    by_category: function(callback) {
      console.info(colors.cyan('Get User Result : BY CATEGORY'));
      new ResultLib(version).getByCategoryResult(req.params.id, callback);
    },
    common_questions: function(callback) {
      console.info(colors.cyan('Get User Result : COMMON QUESTIONS'));
      resultModel.answerPointsSummary({user_id: req.params.id, common: 1}, function(error, result){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          result = result[0];
          var counts = parseInt(result.count);
          var t = {
            // '1': parseFloat(result.sum_1) / counts,
            '2': parseFloat(result.sum_2) / counts,
            '3': parseFloat(result.sum_3) / counts,
            // '4': parseFloat(result.sum_4) / counts,
            '5': parseFloat(result.sum_5) / counts,
            // '6': parseFloat(result.sum_6) / counts,
            '7': parseFloat(result.sum_7) / counts,
            '8': parseFloat(result.sum_8) / counts
          }
          callback(null, t);
        }
      });
    },
    detail_questions: function(callback) {
      console.info(colors.cyan('Get User Result : DETAIL QUESTIONS'));
      resultModel.answerPointsSummary({user_id: req.params.id, common: 0}, function(error, result){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          result = result[0];
          var counts = parseInt(result.count);
          var t = {
            // '1': parseFloat(result.sum_1) / counts,
            '2': parseFloat(result.sum_2) / counts,
            '3': parseFloat(result.sum_3) / counts,
            // '4': parseFloat(result.sum_4) / counts,
            '5': parseFloat(result.sum_5) / counts,
            // '6': parseFloat(result.sum_6) / counts,
            '7': parseFloat(result.sum_7) / counts,
            '8': parseFloat(result.sum_8) / counts
          }
          callback(null, t);
        }
      });
    }
  }, function(error, result){
    console.info(colors.cyan('Get User Result : RESULT'));
    if (error) {
      console.log(error);
      return res.send(error.code, error);
    }
    var o = result;


    var candidates = result.candidates;
    var by_category = result.by_category;

    for (var i in candidates) {
      var candidate = candidates[i];
      var c_id = candidate.id;
      candidate.result = {};

      // Total
      candidate.result.total = result.total[c_id+''];

      // By Candidate
      candidate.result.by_category = {};
      for (var j in by_category) {
        var c = by_category[j];
        candidate.result.by_category[j] = c[c_id+''];
      }

      // Common Question
      candidate.result.common_questions = result.common_questions[c_id+''];

      // Detail Question
      candidate.result.detail_questions = result.detail_questions[c_id+''];

    }

    delete o.candidates;
    
    res.send({candidates: candidates, result:o});
  });

};


/*
 * GET users/:id/result/image - Get user result image
 *
 */

User.prototype.getUserResultImage = function(req, res, next) {
  const CandidateModel = require('../models/candidate');

  var user_id = req.params.id;
  var candidateModel = new CandidateModel(global.version);
  var canvas = new Canvas(1200, 630);
  var ctx = canvas.getContext('2d');

  async.series({
    candidates: function(callback) {
      candidateModel.candidates({excludes:[1,4,6]}, function(error, result){
        callback(error, result);
      });
    },
    total: function(callback) {
      new ResultLib(global.version).getTotalResult(user_id, callback);
    },
    by_category: function(callback) {
      new ResultLib(global.version).getByCategoryResult(user_id, callback);
    },
    labels: function(callback) {
      var labels = null;
      var cache_key = `data_v${global.version}`;
      var data = cache.get(cache_key);
      if (data) {
        console.log('cached data');
        labels = data.categories.map(function(item){ return item.name; });
        callback(null, labels);
      } else {
        const CategoryModel = require('../models/category');
        var categoryModel = new CategoryModel(global.version);
        categoryModel.categories({}, function(err, result) {
          if (result) {
            delete result.info;
            labels = result.map(function(item){ return item.name; });
          }
          callback(err, labels);
        });
      }
    }
  }, function(err, result) {
    if (err) {
      res.send(500, err);
      return;
    }

    const total = result.total;
    const by_category = result.by_category;
    const labels = result.labels;
    const candidates = _.sortBy(result.candidates, function(c) {
      return -total[c.id];
    });

    return imageLib.readImageFile(__dirname + '/../images/graph/bg.png')
    .then(function(bg) {
      ctx.fillStyle = 'black';
      ctx.drawImage(bg, 0, 0, 1200, 630);

      var old_rank = 0;
      return Promise.all(_.map(candidates, function(c, index) {
        var x = 400 * (index % 3);
        var y = index >= 3 ? 314 : 0 ;
        var categories = _.map(by_category, function(val, key) {
          return val[c.id];
        });
        var rank;
        if (index === 0 || total[candidates[index-1].id] != total[c.id]) {
          rank = index + 1;
          old_rank = rank;
        } else {
          rank = old_rank;
        }

        return imageLib.drawResult({
          ctx: ctx,
          baseX: x,
          baseY: y,
          rank: rank,
          candidate: {
            id: c.id,
            name: c.name,
          },
          total: total[c.id],
          categories: categories,
          labels: labels
        });
      }));
    }).then(function() {
      var fileHelper = new FileHelper;
      var stream = canvas.jpegStream();
      var buffers = [];
      stream.on("data", function(chunk) {
        buffers.push(chunk);
      });
      stream.on("end", function() {
        var buffer = Buffer.concat(buffers);
        fileHelper.saveAttachment({
          name: hashid.encode(parseInt(Math.random() * 99999999)) + ".jpg",
          stream: buffer,
          mimetype: "image/jpeg",
        }, function(err, result) {
          if (err) throw err;
          res.send(200, {
            url: `https://s3.ap-northeast-2.amazonaws.com/images.nudepresident.com/${result[0].key}`,
          });
        });
      });
    }).catch(function(err) {
      res.send(500, err);
      return;
    });
  });
}




/*
 * POST users/:id/reset_data  - Reset user data
 *
 */
User.prototype.resetUserData = function(req, res, next) {
  console.info(colors.cyan('Reset User Data'));

  const CategoryModel = require('../models/category');
  const TagModel = require('../models/tag');
  const AnswerModel = require('../models/answer');
  const PromiseModel = require('../models/promise');

  var userModel = new UserModel(global.version);

  async.waterfall([
    function validate(callback) {
      console.info(colors.cyan('Reset User Data : VALIDATE'));

      // Check permission
      if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
        return callback({code:403, description:"forbidden"});

      callback(null, req.params);
    },
    function deleteCandidates(params, callback) {
      console.info(colors.cyan('Reset User Data : DELETE CANDIDATES'));

      var type = params.type;
      var test = { user_id: params.id };
      var null_fields = ['current_candidate','final_candidate','favorite_promise','dislike_promise','final_reason'];

      userModel.updateTest({test: test, null_fields: null_fields}, function(error, info) {
        if (error){
          callback({code:500,description:"Database Error ",error:error}, null);
        } else {
          console.log(info, 'info');
          callback(null, params);
        }
      });
    },
    function deleteAnswers(params, callback) {
      console.info(colors.cyan('Reset User Data : DELETE ANSWERS'));

      var answerModel = new AnswerModel(global.version);

      answerModel.deleteUserAnswers({user_id: params.id}, function(error, info){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          callback(null, params);
        }
      });
    },
    function deleteCategories(params, callback) {
      console.info(colors.cyan('Reset User Data : DELETE CATEGORY'));

      var categoryModel = new CategoryModel(global.version);

      categoryModel.deleteUserCategory({user_id: params.id}, function(error, info){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          callback(null, params);
        }
      });
    },
    function deleteTags(params, callback) {
      console.info(colors.cyan('Reset User Data : DELETE CATEGORY'));

      var tagModel = new TagModel(global.version);

      tagModel.deleteUserTag({user_id: params.id}, function(error, info){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          callback(null, params);
        }
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, true);
    } else {
      res.send(error.code, error);
    }
  });
};



/*
 * GET users/total  total count of users
 */
User.prototype.getTotalCount = function(req, res, next) {

  var userModel = new UserModel(global.version);

  userModel.total({}, function(error, result){
    if (error) {
      res.send(500, {description:"Database Error ",error:error});
    } else {
      res.send(200, {total: parseInt(result.total)});
    }
  });
};

User.prototype.getStats = function(req, res, next) {

  var userModel = new UserModel(global.version);
  var tagModel = new TagModel(global.version);
  var methodWithCache = function(method, cacheKey, callback) {
    cacheKey = cacheKey + global.version; // content version

    var cached = cache.get(cacheKey);
    if (cached) {
      callback(null, cached);
    } else {
      method(function (err, data) {
        if (err) {
          callback(err);
        } else {
          cache.put(cacheKey, data, 1000 * 60 * 5); // cache 5 min
          callback(null, data);
        }
      });
    }
  }

  userModel.user({id: req.params.id}, function(err, user){
    if (err) return callback(err);
    async.parallel({
      total: function(callback) {
        methodWithCache(function (cb) {
          userModel.total({}, cb);
        }, 'total', callback);
      },
      send_message_total: function(callback) {
        methodWithCache(function (cb) {
          userModel.messageTotal({sent_message: true}, cb);
        }, 'send_message_total', callback);
      },
      age_counts: function(callback) {
        methodWithCache(function (cb) {
          userModel.countByAge({}, cb);
        }, 'age_counts', callback);
      },
      gender_counts: function(callback) {
        methodWithCache(function (cb) {
          userModel.countByGender({}, cb);
        }, 'gender_counts', callback);
      },
      job_counts: function(callback) {
        methodWithCache(function (cb) {
          userModel.countByJob({}, cb);
        }, 'job_counts', callback);
      },
      age_tags_top3: function(callback) {
        methodWithCache(function (cb) {
          tagModel.tagsTop3({age: user.age}, cb)
        }, 'age_tags_top3_' + user.age, callback);
      },
      gender_tags_top3: function(callback) {
        methodWithCache(function (cb) {
          tagModel.tagsTop3({gender: user.gender}, cb)
        }, 'gender_tags_top3_' + user.gender, callback);
      },
      job_tags_top3: function(callback) {
        methodWithCache(function (cb) {
          tagModel.tagsTop3({job: user.job}, cb)
        }, 'job_tags_top3_' + user.job, callback);
      },
    }, function(error, result) {
      if (!error) {
        result.age = user.age;
        result.gender = user.gender;
        result.job = user.job;
        res.send(200, result);
      } else {
        res.send(500, error);
      }
    });
  });
};



/*
 * GET users/:id/personalized_tags
 */
User.prototype.getTags = function(req, res, next) {


  async.waterfall([
    function user(callback) {
      var userModel = new UserModel(global.version);
      userModel.user({id: req.params.id}, function(error, user){
        if (error) {
          callback({code: 500, message: 'Database error', error: error}, null);
        } else if (user == undefined) {
          callback({code: 404, message: 'Not found user'}, null);
        } else {
          callback(null, user);
        }
      });
    },
    function tag(user, callback) {
      // 태그 매핑
      var UserMatrixColumn = require('../lib/user_matrix');
      var columns = UserMatrixColumn(user);

      console.log(columns, 'COLUMNS');      
      var tagModel = new TagModel(global.version);
      tagModel.matrix({columns: columns}, function(error, tags){
        if (error) {
          callback({code: 500, message: 'Database error', error: error}, null);
        } else  {
          callback(null, tags);
        }
      });

    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/*
 * GET users/:id/tests
 */
User.prototype.getTests = function(req, res, next) {
  const CandidateModel = require('../models/candidate');
  const TestModel = require('../models/test');
  var testModel = new TestModel(global.version);

  async.waterfall([
    function database(callback) {
      testModel.schemas({}, function(error, result){
        if (error)
          callback({code:500, error: {description: 'Database Error', error: error}}, null);
        else 
          callback(null, result)
      });
    },
    function userTests(schemas, callback) {
      var ids = [];
      schemas.forEach(function(obj){ids.push(obj['Database (nudep_v%)'])});
      testModel.userTests({user_id: req.params.id, schemas:ids}, function(error, result){
        if (error)
          callback({code:500, error: {description: 'Database Error', error: error}}, null);
        else {
          delete result.info;
          callback(null, result);
        }
      });
    },
    function rank(tests, callback) {
      async.each(tests, function(test, cb) {
        async.series({
          candidates: function(cb__) {
            var candidateModel = new CandidateModel(test.version);
            candidateModel.candidates({excludes:[1,4,6]}, function(error, result){
              cb__(error, result);
            });
          },
          total: function(cb__) {
            new ResultLib(test.version).getTotalResult(req.params.id, cb__);
          }
        }, function(error, result){
          if (error) {  return cb(error); }

          var candidates = result.candidates;

          for (var i in candidates) {
            var candidate = candidates[i];
            var c_id = candidate.id;
            candidate.result = {};
            candidate.result.total = result.total[c_id+''];
          }

          candidates.sort(function(a, b) {
            return b.result.total > a.result.total;
          });

          test.candidates = candidates;
          
          cb();
        });
          
      }, function(err) {
          callback(err, tests);
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/*
 * GET users/:id/personalized_tags
 */
User.prototype.getData = function(req, res, next) {

  var version = Number.isInteger(req.params.v) ? req.params.v : global.version;

  var User = require('../object/user');

  var userModel = new UserModel(version);

  userModel.user({id: req.params.id, simple: true, auth: true}, function(error, user){
    if (error)
      res.send({code:500, error: {description: 'Database Error', error: error}}, null);
    else if (user) {
      async.series(new User(version).data({id:req.params.id}), function(error, result){
        if (!error) {
          var data = _.merge(user.data, result);
          data.version = version;
          res.send(200, data);
        } else {
          res.send(error.code, error);
        }
      });
    }
    else
      res.send({code:404, error:{description: 'Not found'}}, null);
  });   

};