/*
 * libraries 
 */
var colors = require('colors');
var passport = require('passport');
var async = require('async');
var jwt = require('jwt-simple');
var UserModel = require('../models/user');
var AuthHelper = require('../lib/authorization');
var request = require('request');
var moment = require('moment');
var base64 = require('../util/base64');
// var twilio_conf = require('../twilio.config');
var FACEBOOK_APP_ID = process.env.FB_APP_ID;
var FACEBOOK_APP_SECRET = process.env.FB_APP_SECRET;

var NAVER_APP_ID = process.env.NAVER_APP_ID;
var NAVER_APP_SECRET = process.env.NAVER_APP_SECRET;

// var smsVerifySecret = 'NudePresidentSMSAuth';
// var secret = "nudepresident_find_password";

/*
 * class declaration
 */


function Auth() {}


/*
 * routing
 */

var auth = new Auth();

module.exports.route = function(app) {
  app.get('/', auth.index);
  app.post('/v1/auth/facebook', auth.facebook);
  app.post('/v1/auth/naver', auth.naver);
  app.post('/v1/auth/token', auth.token);
  app.get('/v1/auth/token', auth.token);
  // app.post('/auth/login', auth.login);
  // app.post('/auth/email_signup', auth.emailSignup);
  // app.post('/auth/send_sms', auth.sendSms);
  // app.post('/auth/verify_phone', auth.verifyPhone);
  // app.post('/auth/update_password', auth.updatePassword);

  // Test
  app.get('/v1/auth/fb_token', checkFBToken);
  // app.get('/auth/twilio_key', auth.getTwilioApiKey);
};


/*
 * controller functions
 */


// Facebook Auth

/**
 *
 * @param token
 * @param callback - (error, {user, profileimage})
 */

function fetchUserFromFacebook(token, cb){

  var FB = require('facebook-node');
  FB.setApiVersion("v2.8");
  FB.setAccessToken(token);

  async.series({
    user: function(callback){
      FB.api('/me', { fields: ['email', 'name', 'birthday'] }, function (result) {
        if(!result || result.error) {
          console.log(!result ? 'error occurred' : result.error);
          callback(result.error, null);
        } else {
          callback(null, result);
        }
      });
    },
    profilephoto: function(callback){
      FB.api('me/picture',{width:320, redirect:false}, function (result) {
        if(!result || result.error) {
          console.log(!result ? 'error occurred' : result.error);
          callback(result.error, null);
        } else {
          callback(null, result.data);
        }
      });
    }
  }, function(error, result){
    if (error) {
      cb(error, null);
    } else {
      console.log('fetchUserFromFacebook', result);
      cb(null, result);
    }
  });
}


/**
 *
 * @param {number} id
 * @param {string} token
 * @param callback - (error, credential)
 */

function verifyFacebookToken(token, callback) {

  request('https://graph.facebook.com/v2.5/debug_token?input_token='+token+'&access_token='+FACEBOOK_APP_ID+'|'+FACEBOOK_APP_SECRET, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // Show the HTML for the Google homepage. 
      var res = JSON.parse(body);
      callback(null, res.data);
    } else {
      callback(error, null)
    }
  });
}


/**
 * Auth with Facebook
 *
 * @param {callback} cb - (error, user)
 */

function authWithFacebook(req, res, cb) {

  var userModel = new UserModel(global.version);
  var created = false;
  var referrer = req.params.referrer;
  
  var BearerStrategy = require('passport-http-bearer').Strategy;
  passport.use(new BearerStrategy(
    function(token, done) {

      async.waterfall([
        // Verify Token
        function verifyToken(callback){
          console.info(colors.cyan('Auth with Facebook: VERIFY TOKEN'));
          verifyFacebookToken(token, function(error, credential){
            if (error){
              callback({code:401,message:"Facebook Token verification failed",error:error}, null);
            } else {
              if (credential.is_valid) {
                callback(null, credential);
              } else {
                callback({code:401,message:"Invalid Access Token"}, null);
              }
            }
          });
        },

        // Get user Id
        function user(credential, callback){
          console.info(colors.cyan('Auth with Facebook: GET USER ID'));

          userModel.userId({facebook_id: credential.user_id}, function(error, user){
            callback(error, user, credential);
          });
        },

        // Update or Create Account
        function createOrUpdateAccount(user, credential, callback){
          console.log('User : ', user);


          /////////////////////////////////////////////////////////////////////////////////////
          /*                               Internal Function                                 */
          /////////////////////////////////////////////////////////////////////////////////////

          // Create User Auth
          function createUserAuth(user_id, callback_){
            console.info(colors.cyan('Auth with Facebook: CREATE USER AUTH'));

            var auth = {};
            auth.user_id = user_id;
            auth.type = 'FACEBOOK';
            auth.identifier = credential.user_id;
            auth.credential_type = 'TOKEN';
            auth.credential = token;
            auth.expires = credential.expires_at;

            userModel.createAuth({auth:auth}, function(error, auth_id){
              if (error) {
                userModel.delete({id: user_id}, function(err, info){
                  callback_({code:500,description:"Couldn't create user auth",error:error}, null);
                });
              } else {
                callback_(null, user_id);  
              }
            });
          }
          /////////////////////////////////////////////////////////////////////////////////////

          // Existing User
          if (user && user.id !== undefined) {
            console.info(colors.cyan('Auth with Facebook: EXSITING USER'));

            // Check Auth & Test
            async.waterfall([
              function getAuth(cb_) {
                var auth = {
                  user_id: user.id,
                  type: 'FACEBOOK'
                }
                userModel.auth({auth:auth}, function(error, result){
                  if (error)
                    cb_({code:500, description:'Database Error', error}, null);
                  else
                    cb_(null, result);
                });
              },
              function createOrUpdateAuth(auth, cb_) {

                if (auth) {
                  // Update Auth
                  console.info(colors.cyan('Auth with Facebook: UPDATE USER AUTH'));

                  var auth = {
                    user_id: user.id,
                    type: 'FACEBOOK',
                    identifier: credential.user_id,
                    credential_type: 'TOKEN',
                    credential: token,
                    expires: credential.expires_at
                  };
                  userModel.updateAuth({auth:auth}, function(error, info){
                    if (!error)
                      cb_(null, user.id);
                    else
                      cb_({code:500,description:"Couldn't update user auth",error:error}, null);
                  });
                } else {
                  // Create Auth
                  console.info(colors.cyan('Auth with Facebook: CREATE EXSISTING USER AUTH'));

                  createUserAuth(user.id, cb_);
                }
              },
              function getTest(user_id, cb_){
                userModel.test({user_id:user_id}, function(error, result){
                  if (error)
                    cb_({code:500, description:'Database Error', error}, null);
                  else
                    cb_(null, result, user_id);
                });
              },
              function createIfNotExist(test, user_id, cb_) {
                if (!test) {
                  userModel.createTest({test:{user_id:user_id}}, function(error, insertId){
                    if (error)
                      cb_({code:500, description:'Database Error', error}, null);
                    else
                      cb_(null, user_id);
                  });
                } else {
                  cb_(null, user_id);
                }
              }

            ], function(error, result) {
              if (!error)
                callback(null, result);
              else
                callback(error, null);
            });
          }

          // New User
          // Create User Account
          else {

            console.info(colors.cyan('Auth with Facebook: NEW USER'));

            async.waterfall([
              // Fetch Facebook User Info
              function fetchFacebookUserInfo(callback_){
                console.info(colors.cyan('Auth with Facebook: FETCH USER INFO FROM FACEBOOK'));

                fetchUserFromFacebook(token, function(err, result) {
                  if (err){
                    console.log('fetchUserFromFacebook error', err);
                    callback_({code:500,description:"Failed to fetch user info from Facebook", error:err}, null);
                  } else {
                    callback_(null, result);
                  }
                });
              },
              // Create User
              function createUser(result, callback_){
                console.info(colors.cyan('Auth with Facebook: CREATE USER'));

                // if (!result.user.email) {
                //   callback_({code: 403, description: 'Not able to access user email address from fb', reason:'no_user_email'}, null);
                //   return;
                // }

                var user = {
                  facebook_id: result.user.id,
                  name: result.user.name,
                  email: result.user.email || null,
                  profileimage: result.profilephoto.url,
                  referrer: referrer || null
                };

                if (result.user.birthday) {
                  user.birthday = result.user.birthday;
                }

                userModel.create({user: user}, function(error, user_id){
                  if (error) {
                    callback_({code:500,description:"Couldn't create user account",error:error}, false);
                  } else {
                    callback_(null, user_id);
                  }
                });

              },
              // Create User Auth
              function createAuth(user_id, callback_){
                createUserAuth(user_id, callback_);
              },
              // Create User Test
              function createUserTest(user_id, callback_) {
                userModel.createTest({test:{user_id: user_id}}, function(error, result) {
                  if (error) {
                    callback_({code:500,description:"Couldn't create user test",error:error}, false);
                  } else {
                    callback_(null, user_id);
                  }
                })
              }
            ], function(error, user_id, email){
              if (error) {
                callback(error, null);
              } else {

                // New User Flag
                created = true;

                callback(null, user_id);
              }
            });
          }
        },
        // Get User Object
        function fetch(user_id, callback) {
          console.info(colors.cyan('Auth with Facebook: FETCH USER'));

          var UserFactory = require('../object/user');
          new UserFactory(global.version).fetch(user_id, callback);
        },
        function matchingPoints(user, callback) {
          var UserFactory = require('../object/user');
          new UserFactory(global.version).matchingPoints(user.id, function(error, result){
            if (result) {
              user.matching_points = result;
            } 
            callback(error, user);
          });
        }
      ], function(error, user){
        if (error) {
          console.log(error, 'authWithFacebook Error')
          done(error, null);
          return;
        }

        user.signed_up = created;
        done(null, user);
      });
    }
  ));

  passport.authenticate('bearer', {session: false}, function(err, user, info) {

    if (err || !user) {
      cb(err, false);
      return;
    }

    cb(null, user);

  })(req, res);

  
}


function checkFBToken(req, res, next) {
  var token = req.params.token;

  fetchUserFromFacebook(token, function(error, result){
    if (error) {
      res.send(500, error);
    } else {
      res.send(200, result);
    }
  });
}




// Naver Auth



/**
 * Auth with Naver
 *
 * @param {Object} req.params
 * @param {string} req.params.auth = 'naver'
 * @param {string} req.params.token
 * @param {callback} cb - (error, user)
 */

function authWithNaver(req, res, cb) {
  var userModel = new UserModel(global.version);

  var code = req.params.code;
  var state = req.params.state;
  var token;
  var created = false;
  var referrer = req.params.referrer;


  async.waterfall([
    function getNaverToken(callback) {

      request('https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id='+NAVER_APP_ID+'&client_secret='+NAVER_APP_SECRET+'&code='+code+'&state='+state, function (error, response, body) {
        console.log('NAVER API: ', body);
        if (response.statusCode == 200) {
          var res = JSON.parse(body);
          console.log(res);
          if (res.access_token !== undefined) {
            token = res.access_token;
            callback(null, res.access_token);
          }
          else
            callback({code: 401, message: 'Not able to get naver token'});
        } else {
          var e = {
            code: response.statusCode,
            message: body
          }
          callback(e, null);
        }
      });
    },
    function getNaverUser(token, callback) {
      var options = {
        url: 'https://openapi.naver.com/v1/nid/me',
        headers: {
          'Authorization': 'Bearer ' + token 
        }
      };

      request(options, function (error, response, body) {
        console.log('NAVER API: ', body);
        if (response.statusCode == 200) {
          var res = JSON.parse(body);
          callback(null, res.response);
        } else {
          var e = {
            code: response.statusCode,
            message: body
          }
          callback(e, null);
        }
      });
    },
    function getUser(naverUser, callback) {
      userModel.userId({naver_id: naverUser.id}, function(error, user){
        callback(error, user, naverUser);
      });
    },
    function createOrUpdateUser(user, naverUser, callback) {
      console.log('Naver createOrUpdateUser');

      console.log('naverUser : ', naverUser);
      
      // Existing User
      if (user && user.id !== undefined) {
        // Check Auth Exists
        async.waterfall([
          function getAuth(cb_) {
            var auth = {
              user_id: user.id,
              type: 'NAVER',
              identifier: naverUser.id,
              credential_type: 'TOKEN',
              credential: token
            };
            userModel.auth({auth:auth}, function(error, result){
              if (!error)
                cb_(null, result);
              else
                cb_({code:500,message:"Couldn't update user auth",error:error}, null);
            });
          },
          function createOrUpdateAuth(auth, cb_) {

            if (auth) {
              // Update Auth
              console.info(colors.cyan('Auth with Naver: UPDATE USER AUTH'));

              var auth = {
                user_id: user.id,
                type: 'NAVER',
                identifier: naverUser.id,
                credential_type: 'TOKEN',
                credential: token,
              };
              userModel.updateAuth({auth:auth}, function(error, info){
                if (!error)
                  cb_(null, user.id);
                else
                  cb_({code:500,description:"Couldn't update user auth",error:error}, null);
              });
            } else {
              // Create Auth
              console.info(colors.cyan('Auth with Naver: CREATE EXSISTING USER AUTH'));

              createUserAuth(user.id, cb_);
            }
          },
          function getTest(user_id, cb_){
            userModel.test({user_id:user_id}, function(error, result){
              if (error)
                cb_({code:500, description:'Database Error', error}, null);
              else
                cb_(null, result, user_id);
            });
          },
          function createIfNotExist(test, user_id, cb_) {
            if (!test) {
              userModel.createTest({test:{user_id:user_id}}, function(error, insertId){
                if (error)
                  cb_({code:500, description:'Database Error', error}, null);
                else
                  cb_(null, user_id);
              });
            } else {
              cb_(null, user_id);
            }
          }

        ], function(error, result) {
          if (!error)
            callback(null, result);
          else
            callback(error, null);
        });

      } else { // New user
        async.waterfall([
          function checkUser(callback_) {
            userModel.userId({email: naverUser.email}, function(error, user){
              if (error) {
                return callback_({code: 500, message: 'Database error', error: error}, null);
              }
              if (user != undefined) {
                callback_({code: 409, message: 'User email already is used for Facebook signup : '+ naverUser.email}, null);
              } else {
                callback_(null, true);
              }
            });
          },
          function createUser(available, callback_) {
            console.log('Create User');
            if (!naverUser.email) {
              callback_({code: 401, message: 'Not able to access user email address from naver'})
            }

            var user = {
              name: naverUser.name,
              email: naverUser.email,
              profileimage: naverUser.profile_image,
              referrer: referrer || null
            };

            userModel.create({user: user}, function(error, user_id){
              if (error) {
                callback_({code:500,message:"Couldn't create user account",error:error}, false);
              } else {
                // New User Flag
                created = true;

                callback_(null, user_id);
              }
            });
          },
          function auth(user_id, callback_) {
            console.log('Create User Auth');
            var auth = {};
            auth.user_id = user_id;
            auth.type = 'NAVER';
            auth.identifier = naverUser.id;
            auth.credential_type = 'TOKEN';
            auth.credential = token;

            userModel.createAuth({auth:auth}, function(error, auth_id){
              if (error) {
                userModel.delete({id: user_id}, function(err, info){
                  callback_({code:500,message:"Couldn't create user auth",error:error}, null);
                });
              } else {
                callback_(null, user_id);  
              }
            });
          },
          // Create User Test
          function createUserTest(user_id, callback_) {
            userModel.createTest({test:{user_id: user_id}}, function(error, result) {
              if (error) {
                callback_({code:500,description:"Couldn't create user test",error:error}, false);
              } else {
                callback_(null, user_id);
              }
            })
          }
        ], function(err, user_id){ 
          if (err) {
            callback(error, null);
          } else {
            callback(null, user_id);
          }
        });
      }
    },
    // Get User Object
    function fetch(user_id, callback) {
      console.info(colors.cyan('Auth with Facebook: FETCH USER'));

      var UserFactory = require('../object/user');
      new UserFactory(global.version).fetch(user_id, callback);
    },
    function matchingPoints(user, callback) {
      var UserFactory = require('../object/user');
      new UserFactory(global.version).matchingPoints(user.id, function(error, result){
        if (result) {
          user.matching_points = result;
        } 
        callback(error, user);
      });
    }
  ], function(error, user){
    if (error || !user) {
      cb(error, false);
      return;
    }

    user.signed_up = created;
    cb(null, user);
  });


}



Auth.prototype.facebook = function(req, res, next) {

  function finishHandler(error, user) {

    console.log(user);

    if (error || !user) {
      res.send(401, {description:'Unauthorized', error: error});
    } else {
      var tokenObj = AuthHelper.generateToken(req, user);

      tokenObj.user = user;
      tokenObj.signed_up = user.signed_up;
      delete user.signed_up;

      res.send(200, tokenObj);
    }
    
  }

  return authWithFacebook(req, res, finishHandler);
  
};


Auth.prototype.naver = function(req, res, next) {

  function finishHandler(error, user) {

    console.log(user);

    if (error || !user) {
      res.send(401, {description:'Unauthorized', error: error});
    } else {
      var tokenObj = AuthHelper.generateToken(req, user);

      tokenObj.user = user;
      tokenObj.signed_up = user.signed_up;
      delete user.signed_up;

      res.send(200, tokenObj);
    }
    
  }

  return authWithNaver(req, res, finishHandler);
  
};



Auth.prototype.token = function(req, res, next) {

  var BearerStrategy = require('passport-http-bearer').Strategy;
  passport.use(new BearerStrategy(
    function(token, done) {

      var userModel = new UserModel(global.version);

      var UserFactory = require('../object/user');

      // Validation
      async.waterfall([
        function user(callback) {
          var user = AuthHelper.userFromToken(token);
          if ('production' == process.env.NODE_ENV && (!AuthHelper.validate(token, req) || !user)) {
            return callback({code:401, error:{description:'Unauthorized'}}, null);
          } 

          new UserFactory(global.version).fetch(user.id, callback);   
        },
        function getTest(user, cb_){
          userModel.test({user_id:user.id}, function(error, result){
            if (error)
              cb_({code:500, description:'Database Error', error}, null);
            else
              cb_(null, result, user);
          });
        },
        function createIfNotExist(test, user, cb_) {
          if (!test) {
            userModel.createTest({test:{user_id:user.id}}, function(error, insertId){
              if (error)
                cb_({code:500, description:'Database Error', error}, null);
              else
                cb_(null, user);
            });
          } else {
            cb_(null, user);
          }
        },
        function matchingPoints(user, callback) {
          new UserFactory(global.version).matchingPoints(user.id, function(error, result){
            if (result) {
              user.matching_points = result;
            } 
            callback(error, user);
          });
        },
        function renewToken(user, callback) {
          var newToken = AuthHelper.generateToken(req, user);
          newToken.user = user;
          callback(null, newToken);
        }
      ], function(error, result) {
        done(error, result);
      });

    }
  ));

  passport.authenticate('bearer', {session: false}, function(err, user, info) {

    if (err || !user) {
      res.send(err.code, err.error);
      return;
    }
    res.send(200, user);

  })(req, res);

};




Auth.prototype.index = function(req, res, next) {
  res.send({
    result: true,
    data: "index"
  });
};
