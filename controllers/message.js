/*
 * libraries 
 */
const async = require('async');
const colors = require('colors');
const UserModel = require('../models/user');
const cache = require('memory-cache');


/*
 * class declaration
 */


function Message() {}


/*
 * routing
 */

var message = new Message();

module.exports.route = function(app) {
  app.get('/v1/messages/recent', message.getMessages);
};


/*
 * controller functions
 */


Message.prototype.index = function(req, res, next) {
  res.send({
    result: true,
    data: "index"
  });
};



/* Get list of messages
 * @param   {Object}  params
 * @param   {number}  params.offset
 * @param   {number}  params.limit
 * @param   {boolean}  params.has_message
 * @param   {fn}      callback(err, result)
 */
Message.prototype.getMessages = function(req, res, next) {

  // Cache
  var data = cache.get(`message${global.version}`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }


  var userModel = new UserModel(global.version);

  var offset = 0;
  var limit = 10;

  userModel.users({
    has_message:true, 
    order: {by:'sent_message',asc:false},
    offset: offset,
    limit: limit,
    location: true,
    with_final_candidate: true
  }, function(error, messages){
    if (error) {
      res.send(500, {description: 'Databse error'});
    } else {
      delete messages.info;
      var m = [];
      messages.forEach(function(item, i){
        m.push({
          message: item.message,
          sent: item.sent_message,
          candidate: item.candidate,
          user: {
            age: item.age,
            job: item.job,
            location: {
              district: item.district
            }
          }
        });
      });
      cache.put(`message${global.version}`, m, 10000); // 10초
      res.send(200, m);
    }
  });
};
