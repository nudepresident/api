if ('production' == process.env.NODE_ENV)
  require('newrelic');

var restify = require('restify');
var fs = require('fs');
var logger = require('./lib/logger');
var passport = require('passport');
var session = require('./lib/session');
var authenticate = require('./authenticate');
var authorization = require('./lib/authorization');
var CookieParser = require('restify-cookies');
var corsMiddleware = require('restify-cors-middleware');
var requestIp = require('request-ip');


var server = restify.createServer({
  name: 'Nudepresident API v2',
  versions: [ '1.0.0', '2.0.0', '3.0.0', '4.0.0', '5.0.0', '6.0.0', '7.0.0', '8.0.0']
});

server.on('after', restify.auditLogger({
  log: logger
}));


server.use(requestIp.mw());
server.use(CookieParser.parse);
server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.gzipResponse());
server.use(restify.throttle({
  burst: 100,
  rate: 50,
  ip: true, // throttle based on source ip address
  overrides: {
    '127.0.0.1': {
      rate: 0, // unlimited
      burst: 0
    }
  }
}));


// CORS
var cors = corsMiddleware({
  origins: [
    'http://dev.nudepresident.com', 
    'https://www.nudepresident.com',
    'https://nudepresident.com',
    'http://nudepresident.com'
    ],
  allowHeaders: [
    'Authorization', 
    'Set-Cookie',
    'X-ACCESS-TOKEN',
    'x-access-token',
    'postman-token',
    'cache-control',
    'accept-version'
    ]
});

server.pre(cors.preflight);
server.use(cors.actual);


server.pre(function (req, res, next) {
  
  // DB Version
  var av = req.headers['accept-version'];
  var version = av ? av.split('.')[0] : undefined;
  global.version = version;

  return next();
});

server.use(function(req, res, next){
  res.header('Access-Control-Allow-Credentials', true);
  res.charSet('utf-8');

  next();
});

// Ping
server.get('/ping', function(req, res, next){
  res.send(200, global.version);
});




// Authorization
server.use(authorization.authorize);


if ('production' != process.env.NODE_ENV)
  server.get('/debug', authorization.debug);


// load route
require('./route.js')(__dirname+'/controllers', server);



server.listen(process.env.PORT || 3333, function startServer() {
  console.log('%s listening at %s', server.name, server.url);
});

