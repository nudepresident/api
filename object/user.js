const colors = require('colors');
const UserModel = require('../models/user');
const AnswerModel = require('../models/answer');
const CategoryModel = require('../models/category');
const PromiseModel = require('../models/promise');
const TagModel = require('../models/tag');
const TestModel = require('../models/test');
const async = require('async');
const _ = require('lodash');


var data = function(user) {

  if (!user.data) user.data = {};

  var version = this.version;

  console.log('========================== DATA VERSION : '+version)

  return {
    common_answers : function(callback_) {
      console.info(colors.cyan('Fething a User : COMMON ANSWER'));
      var answerModel = new AnswerModel(version);

      answerModel.answers({user_id:user.id, common: 1}, function(error, answers){
        if (error)
          callback_({code:500, error: {description: 'Database Error', error: error}}, null);
        else {
          delete answers.info;

          for (var i in answers) {
            var a = answers[i];
            var as = a.answers.split(',');
            a.answer_text = as[a.answer_idx];
            delete a.answers;
          }

          callback_(null, answers);
        }
      });
    },
    // 분야별 질문 답변
    answers_by_category : function(callback_) {
      console.info(colors.cyan('Fething a User : ANSWER BY CATEGORY'));

      var answerModel = new AnswerModel(version);

      answerModel.answers({user_id:user.id, common: 0}, function(error, answers){
        if (error)
          callback_({code:500, error: {description: 'Database Error', error: error}}, null);
        else {
          delete answers.info;

          var abc = {};
          for (var i in answers) {
            var a = answers[i];
            var as = a.answers.split(',');
            var o = abc[''+a.category_id] || [];
            a.answer_text = as[a.answer_idx];
            o.push(a);
            abc[''+a.category_id] = o;
            delete a.category_id;
            delete a.answers;
          }

          callback_(null, abc);
        }
      });
    },
    // 선택 분야
    categories : function(callback_) {
      console.info(colors.cyan('Fething a User : USER CATEGORIES'));

      var categoryModel = new CategoryModel(version);

      categoryModel.userCategories({user_id: user.id}, function(error, categories){
        if (error) {
          callback_({code: 500, description: 'Database error', error: error}, null);
        } else {
          delete categories.info;
          console.log(categories, colors.red('Categories'));
          var c = [];
          for (var i in categories) {
            c.push(categories[i].category_id);
          }

          callback_(null, c);
        }
      });
    },
    // 선택 태그
    tags : function(callback_) {
      console.info(colors.cyan('Fething a User : USER TAGS'));

      var tagModel = new TagModel(version);

      tagModel.userTags({user_id: user.id}, function(error, tags){
        if (error) {
          callback_({code: 500, description: 'Database error', error: error}, null);
        } else {
          delete tags.info;
          console.log(tags, colors.red('Tags'));
          var t = [];
          for (var i in tags) {
            t.push(tags[i].tag_id);
          }
          callback_(null, t);
        }
      });
    },
    // 선택 공약
    promises : function(callback_) {
      console.info(colors.cyan('Fething a User : USER PROMISES'));

      var promiseModel = new PromiseModel(version);

      promiseModel.userPromises({user_id: user.id}, function(error, promises){
        if (error) {
          callback_({code: 500, description: 'Database error', error: error}, null);
        } else {
          delete promises.info;
          var p = [];
          console.log(promises, colors.red('Promises'));
          for (var i in promises) {
            p.push(promises[i].id);
          }
          callback_(null, p);
        }
      });
    }
  }
};

var fetch = function (user_id, callback) {

  console.info(colors.cyan('Fething a User'));

  var userModel = new UserModel(this.version);
  var testModel = new TestModel(this.version);

  const self = this;

  async.waterfall([
    function user(callback_) {
      console.info(colors.cyan('Fething a User : USER'));

      userModel.user({id: user_id, simple: true, auth: true}, function(error, user){
        if (error)
          callback_({code:500, error: {description: 'Database Error', error: error}}, null);
        else if (user) {
          callback_(null, user);
        }
        else
          callback_({code:404, error:{description: 'Not found'}}, null);
      });   
    },
    function database(user, callback_) {
      testModel.schemas({}, function(error, result){
        if (error)
          callback_({code:500, error: {description: 'Database Error', error: error}}, null);
        else 
          callback_(null, result, user)
      });
    },
    function userTests(schemas, user, callback_) {
      var ids = [];
      schemas.forEach(function(obj){ids.push(obj['Database (nudep_v%)'])});
      testModel.userTests({user_id: user.id, schemas:ids}, function(error, result){
        if (error)
          callback_({code:500, error: {description: 'Database Error', error: error}}, null);
        else {
          delete result.info;
          user.tests = result;
          callback_(null, user);
        }
      });
    },
    // // 지역
    // function location(user, callback_) {
    //   console.info(colors.cyan('Fething a User : LOCATION'));

    //   user.data.location = {};
    //   callback_(null, user);
    // },
    // 공통 질문 답변
    function data(user, callback_) {
      async.series(self.data(user), function(err, data){
        user.data = _.merge(user.data, data);
        callback_(err, user);
      });
    }
    ], function(error, result) {

      callback(error, result);

    });
};


var matchingPoints = function(user_id, method_callback) {
  const ResultModel = require('../models/result');

  var resultModel = new ResultModel(this.version);

  console.info(colors.cyan('Get User Result'));

  async.series({
    total: function(callback) {
      console.info(colors.cyan('Get User Result : TOTAL'));

      async.series({
        answerPoints: function(cb) {
          resultModel.answerPointsSummary({user_id: user_id}, function(error, result){
            if (error)
              cb({code:500,description:"Database Error ",error:error}, null);
            else 
              cb(null, result[0]);
          });
        },
        promisePoints: function(cb) {
          resultModel.promisePointsSummary({user_id: user_id}, function(error, result){
            if (error)
              cb({code:500,description:"Database Error ",error:error}, null);
            else 
              cb(null, result[0]);
          });
        }
      }, function(error, result){
        if (error)
          callback(error, null);
        else {
          var counts = parseInt(result.answerPoints.count) + parseInt(result.promisePoints.count);
          var t = {
            // '1': (parseInt(result.answerPoints.sum_1) + parseInt(result.promisePoints.sum_1)),
            '2': (parseInt(result.answerPoints.sum_2) + parseInt(result.promisePoints.sum_2)),
            '3': (parseInt(result.answerPoints.sum_3) + parseInt(result.promisePoints.sum_3)),
            // '4': (parseInt(result.answerPoints.sum_4) + parseInt(result.promisePoints.sum_4)),
            '5': (parseInt(result.answerPoints.sum_5) + parseInt(result.promisePoints.sum_5)),
            // '6': (parseInt(result.answerPoints.sum_6) + parseInt(result.promisePoints.sum_6)),
            '7': (parseInt(result.answerPoints.sum_7) + parseInt(result.promisePoints.sum_7)),
            '8': (parseInt(result.answerPoints.sum_8) + parseInt(result.promisePoints.sum_8)),
          }
          callback(null, {points:t, counts: counts});
        }
      });
      
    },
    common_questions: function(callback) {
      console.info(colors.cyan('Get User Result : COMMON QUESTIONS'));
      resultModel.answerPointsSummary({user_id: user_id, common: 1}, function(error, result){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          result = result[0];
          var counts = parseInt(result.count);
          var t = {
            // '1': parseInt(result.sum_1),
            '2': parseInt(result.sum_2),
            '3': parseInt(result.sum_3),
            // '4': parseInt(result.sum_4),
            '5': parseInt(result.sum_5),
            // '6': parseInt(result.sum_6),
            '7': parseInt(result.sum_7),
            '8': parseInt(result.sum_8)
          }
          callback(null, {points: t, couns: counts});
        }
      });
    },
    detail_questions: function(callback) {
      console.info(colors.cyan('Get User Result : DETAIL QUESTIONS'));
      resultModel.answerPointsSummary({user_id: user_id, common: 0}, function(error, result){
        if (error)
          callback({code:500,description:"Database Error ",error:error}, null);
        else {
          result = result[0];
          var counts = parseInt(result.count);
          var t = {
            // '1': parseInt(result.sum_1),
            '2': parseInt(result.sum_2),
            '3': parseInt(result.sum_3),
            // '4': parseInt(result.sum_4),
            '5': parseInt(result.sum_5),
            // '6': parseInt(result.sum_6),
            '7': parseInt(result.sum_7),
            '8': parseInt(result.sum_8)
          }
          callback(null, {points: t, counts:counts});
        }
      });
    }
  }, function(error, result){
    console.info(colors.cyan('Get User Result : RESULT'));
    if (error) {
      console.log(error);
      return method_callback(error, null);
    }
    
    method_callback(null, result);
  });
};

var jsonFromRecord = function(record) {
  if (!record)
    return null;

  var r = record;

  r.facebook = {
    id: record.facebook_id,
    profileimage: record.profileimage
  }

  delete r.facebook_id;
  delete r.profileimage;
  delete r.current_candidate
  delete r.final_candidate
  delete r.message
  delete r.final_reason
  delete r.favorite_promise
  delete r.dislike_promise

  return r;
}

var User = function(version) {
  this.version = version;
}

User.prototype.data = data;
User.prototype.fetch = fetch;
User.prototype.matchingPoints = matchingPoints;
User.prototype.jsonFromRecord = jsonFromRecord;

module.exports = User;