var session = require('./lib/session');
var url = require('url');
var jwt = require('jwt-simple');
var moment = require('moment');
const async = require('async');

var secret = "nudepresident_find_password";


function sendFindPassword(toEmail, token, callback) {

  if (!toEmail && callback) {
    callback('error');
    return;
  }

  var ses = require('node-ses')
  , client = ses.createClient({ key: process.env.SES_KEY, secret: process.env.SES_SECRET, amazon: 'https://email.us-west-2.amazonaws.com'})
  , fs = require('fs');

  async.series({
    html: function (cb) {
      fs.readFile(__dirname+'/template/find_password.html', 'utf8', function (err, html) {
        cb(err, html);
      });
    },
    text: function (cb1) {
      fs.readFile(__dirname+'/template/find_password.txt', 'utf8', function (err, text) {
        cb1(err, text);
      });
    }
  }, function(error, result) {

    var dest = 'http://nudepresident.com/find_password/?token=';

    var html = result.html.replace(/FIND_PASSWORD_TOKEN/g, dest + token);
    var text = result.text.replace(/FIND_PASSWORD_TOKEN/g, dest + token);

    if (!error) {
      client.sendEmail({
         to: toEmail
       , from: 'admin@nudepresident.com'
       , bcc: null
       , cc: null
       , subject: '누드대통령 비밀번호 찾기'
       , message: html
       , altText: text
      }, function (err, data, res) {
        if (callback)
          callback(err, result, res);
      });
    } else {
      callback(error);
    }
  });
}

module.exports = {
  initialize: function(server) {
    server.get('/auth/get_token', function(req, res, next){

      if (req.params.via_email !== undefined) {

        console.log('/auth/get_token');

        var now = moment().unix();
        var email = req.params.via_email;

        var token = {
          email: email,
          expires: now + (60 * 60 *24) // 1 day
        };

        token = jwt.encode(token, secret);
        sendFindPassword(email, token, function(err, data, resp){
          if (!err) {
            res.send(200, true);
          } else {
            res.send(500, {error:err, token:token});
          }
        });
        return;
      }

      if (!session.isValid(req, res)) {
        var token = session.create(req, res);
        res.send(200, token);
        next();  
      } else {
        if (session.user) {
          var token = session.extend(req, res);
          res.send(200, token);
        }
        next();
      }
    });

    server.get('/auth/logout', function(req, res, next){
      session.clear(req, res);
      session.create(req, res);
      res.send(200, true);
      next();
    });

    server.get('/clear_session', function(req, res, next){
      session.clear(req, res);
      res.send(200, true);
      next();
    });

    server.get('/debug_token', function(req, res, next){
      var token = req.params.token;
      res.send(200, session.decode(token));
    });

  },
  auth: function(req, res, next) {
    console.log('authenticate auth');

    // 유효성 검사
    if (!session.isValid(req, res)) {
      res.send(401, "Not authorized");
      return;
    }

    var skips = ['/auth/login','/auth/email_signup'];

    // 갱신
    var pathname = url.parse(req.url).pathname;
    console.log('Request URI : ' + pathname);
    if( skips.indexOf(pathname) < 0 && session.user) {
      session.extend(req, res);
    }

    //
    next();
  }
};

