const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');



var TestModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};


/* Database
 * @params {fn}       callback(err, result);
 */
TestModel.prototype.schemas = function(params, callback) {

  // Query
  base.get(new Client(base.read_only), "SHOW DATABASES LIKE 'nudep_v%'", callback);

}


/* Tests
 * @params {number}   id
 * @params {fn}       callback(err, result);
 */
TestModel.prototype.tests = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('id')
              .field('title')
              .field('version')
              .field('created')
              .field('deployed')
              .field('due')
              .from('nudep.test');

  if (params.id != undefined) {
    sql = sql.where('id = ?', params.id);
  }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}


/* Test schedule
 * @params {fn}       callback(err, result);
 */
TestModel.prototype.schedule = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('nudep.test_schedule');

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result[0]);
  });

}


/* User Tests
 * @params {number}   user_id
 * @params {array}    versions - nudep_vN
 * @params {fn}       callback(err, result);
 */
TestModel.prototype.userTests = function(params, callback) {
  // Validation
  if (params.user_id == undefined) 
    return callback({error: 'lack of parameter: user_id'}, null);

  if (!Array.isArray(params.schemas) || params.schemas.length < 1) 
    return callback({error: 'lack of parameter: schemas'}, null);


  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('t.title')
              .field('t.version')
              .field('t.created')
              .field('t.deployed')
              .field('t.due')
              .field('ut.modified','responded')
              .field(squel.select({autoQuoteFieldNames: false})
                .field('IF(count(*) > 0, 1, 0)')
                .from(`${params.schemas[0]}.user_tag`)
                .where('user_id = ?', params.user_id)
                .where('selected_promise IS NOT NULL'), 'result')
              .from(`${params.schemas[0]}.user_test`,'ut')
              .join('nudep.test','t')
              .where('user_id = ?', params.user_id)
              .where('t.schema like ?', params.schemas[0])
              .order('ut.created',false)
              .limit(1);

    var sql = `
    (SELECT 
    t.title, t.version, t.created, t.deployed, t.due, ut.modified AS "responded", 
    (SELECT IF(count(*) > 0, 1, 0) FROM ${params.schemas[0]}.user_tag WHERE (user_id = ${params.user_id}) AND (selected_promise IS NOT NULL)) AS "result" 
    FROM ${params.schemas[0]}.user_test ut 
    INNER JOIN nudep.test t 
    WHERE (user_id = ${params.user_id}) 
    AND (t.schema like '${params.schemas[0]}') 
    ORDER BY ut.created DESC 
    LIMIT 1)
    `;

    const reg = /nudep_v([0-9]+)/;
    for (var i = 1; i < params.schemas.length; i++) {
      console.log(params.schemas[i]);
      var n = reg.exec(params.schemas[i])[1];
      sql += `
      UNION (SELECT 
      t.title, t.version, t.created, t.deployed, t.due, ut.modified AS "responded", 
      (SELECT IF(count(*) > 0, 1, 0) FROM ${params.schemas[i]}.user_tag WHERE (user_id = ${params.user_id}) AND (selected_promise IS NOT NULL)) AS "result" 
      FROM ${params.schemas[i]}.user_test ut 
      INNER JOIN nudep.test t 
      WHERE (user_id = ${params.user_id}) 
      AND (t.schema like '${params.schemas[i]}') 
      ORDER BY ut.created DESC 
      LIMIT 1)
      `;
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



module.exports = TestModel;