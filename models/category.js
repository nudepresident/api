const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var CategoryModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};




function validateCategory(category) {
	// Validation
	var error;
	if (!category) 
		return {error: 'lack of parameter : category'};

	if (category.user_id == undefined) 
		return {error: 'lack of parameter : user_id'};

	if (category.category_id == undefined) 
		return {error: 'lack of parameter : category_id'};


	return true;
}


/* Create a category
 * @params {number*} params.category
 * @params {number} params.category.category_id
 * @params {number} params.category.user_id
 * @params {fn}	  	callback(err, result) - result : insertId
 */
CategoryModel.prototype.create = function(params, callback) {

	// Validation
	var category = params.category;
	
	var validMsg = validateCategory(category);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

	var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`nudep${this.version}.user_category`)
              .setFields(category)
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.user);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/* Edit category - modify
 * @params {number*} params.category
 * @params {number} params.category.category_id
 * @params {number} params.category.user_id
 * @params {fn}	  	callback(err, result) - result : info
 */
 /*
CategoryModel.prototype.update = function(params, callback) {

	// Validation
	var category = params.category;
	var validMsg = validateCategory(category);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table(`nudep${this.version}.user_category`)
              .setFields(category)
              .where('user_id = ?', category.user_id)
              .where('question_id = ?', category.question_id);
  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }
  
  // Query
  base.update(c, sql.toString(), callback);

}
*/


/* Category
 * @params {number} params.id
 * @params {fn}     callback(err, result);
 */
CategoryModel.prototype.category = function(params, callback) {
  // Validation
  console.log(params);

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
    					.field('uc.id')
              .field('uc.user_id')
              .field('uc.category_id')
              .field('c.name','category_name')
              .field('uc.created')
              .from(`nudep${this.version}.user_category`,'uc')
              .left_join(`nudep${this.version}.category`,'c','uc.category_id=c.id');

    if (params.category_id)
      sql = sql.where('category_id = ?', params.category_id);

  	if (params.user_id)
  		sql = sql.where('user_id = ?', params.user_id);

  	if (params.id)
  		sql = sql.where('id = ?', params.id);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result[0]);
  });

}





/* Categories
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.category_id
 * @params {array}    params.inIDs - ids array
 * @params {fn}       callback(err, result);
 */
CategoryModel.prototype.categories = function(params, callback, master) {
  // Validation

  var c = new Client(master ? base.db_config : base.read_only);

  try {
    var sql = squel.select()
              .field('c.id')
              .field('c.name')
              .field('c.issues')
              .from(`nudep${this.version}.category`,'c');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.inIDs)
      sql = sql.where('id IN ?', params.inIDs);

    if (params.user_id != undefined)
      sql = sql.left_join('user_category','uc','uc.category_id=c.id')
            .where('uc.user_id = ?', params.user_id)


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}


/* Categories
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.question_id
 * @params {fn}       callback(err, result);
 */
CategoryModel.prototype.userCategories = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('uc.id')
              .field('uc.category_id')
              .from(`nudep${this.version}.user_category`,'uc');

    if (params.id)
      sql = sql.where('uc.id = ?', params.id);

    if (params.user_id)
      sql = sql.where('uc.user_id = ?', params.user_id);

    if (params.question_id) {
      sql = sql.left_join(`nudep${this.version}.question`,'q','q.category_id=uc.category_id')
            .where('q.id = ?', params.question_id);
    }


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



/* Delete a user category
 * @param   {Object}  params
 * @param   {number}  params.user_id
 * @param   {fn}      callback(err, result)
 */
CategoryModel.prototype.deleteUserCategory = function(params, callback) {
  // Validation
  if (params.user_id == undefined) 
    return callback({error: 'lack of parameter: user_id'}, null);

  var c = new Client(base.db_config);

  var sql = squel.delete()
            .from(`nudep${this.version}.user_category`)
            .where('user_id = ?', params.user_id)
            .toString();

  console.log(sql);
  var result = false;
  var error = null;

  c.query(sql, function(err, rows) {
    if (err)
      error = err;
    else {
      result = rows.info;
    }
  });

  c.on('end', function() {
    console.log('delete user category : ', result);
    callback(error, result);
  });

  c.end();
}


module.exports = CategoryModel;