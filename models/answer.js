const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var AnswerModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};


function answerObjectMap(a) {
	if (!a) return null;

	var r = {};
	r.id = a.id;
	r.question = {
		id: a.question_id
	};
	r.answer_idx = a.answer_idx;
	r.answer_text = a.answer_text;
	r.matching_points = {
		// '1': a.matching_point_1,
		'2': a.matching_point_2,
		'3': a.matching_point_3,
		// '4': a.matching_point_4,
		'5': a.matching_point_5,
    // '6': a.matching_point_6
    '7': a.matching_point_7,
    '8': a.matching_point_8
	};
	r.created = a.created;


	return r;
}


function validateAnswer(answer) {
	// Validation
	var error;
	if (!answer) 
		return {error: 'lack of parameter : answer'};

	if (answer.user_id == undefined) 
		return {error: 'lack of parameter : user_id'};

	if (answer.question_id == undefined) 
		return {error: 'lack of parameter : question_id'};

	if (answer.answer_idx == undefined) 
		return {error: 'lack of parameter : answer_idx'};

	// if (answer.matching_point_1 == undefined) 
	// 	return {error: 'lack of parameter : matching_point_1'};

	if (answer.matching_point_2 == undefined) 
		return {error: 'lack of parameter : matching_point_2'};

	if (answer.matching_point_3 == undefined) 
		return {error: 'lack of parameter : matching_point_3'};

	// if (answer.matching_point_4 == undefined) 
	// 	return {error: 'lack of parameter : matching_point_4'};

	if (answer.matching_point_5 == undefined) 
		return {error: 'lack of parameter : matching_point_5'};

  // if (answer.matching_point_6 == undefined) 
  //   return {error: 'lack of parameter : matching_point_6'};

  if (answer.matching_point_7 == undefined) 
    return {error: 'lack of parameter : matching_point_7'};

  if (answer.matching_point_8 == undefined) 
    return {error: 'lack of parameter : matching_point_8'};

	return true;
}


/* Create a answer
 * @params {number*} params.answer
 * @params {number} params.answer.user_id
 * @params {number} params.answer.question_id
 * @params {number} params.answer.answer_idx
 * @params {string} params.answer.answer_text
 * @params {number} params.answer.matching_point_1 - 후보 ID 1 의 매칭 점수 
 * @params {number} params.answer.matching_point_2 - 후보 ID 2 의 매칭 점수 
 * @params {number} params.answer.matching_point_3 - 후보 ID 3 의 매칭 점수 
 * @params {number} params.answer.matching_point_4 - 후보 ID 4 의 매칭 점수 
 * @params {number} params.answer.matching_point_5 - 후보 ID 5 의 매칭 점수
 * @params {number} params.answer.matching_point_6 - 후보 ID 6 의 매칭 점수 
 * @params {number} params.answer.matching_point_7 - 후보 ID 7 의 매칭 점수
 * @params {number} params.answer.matching_point_8 - 후보 ID 8 의 매칭 점수 
 * @params {fn}	  	callback(err, result) - result : insertId
 */
AnswerModel.prototype.create = function(params, callback) {

	// Validation
	var answer = params.answer;
	
	var validMsg = validateAnswer(answer);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

	var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`nudep${this.version}.user_answer`)
              .setFields(answer)
              .set('user_category_id', // 
                squel.select()
                  .field('max(uc.id)')
                  .from(`nudep${this.version}.user_category`,'uc')
                  .left_join(`nudep${this.version}.question`,'q','q.category_id=uc.category_id')
                  .where('q.common = ?', 0) //
                  .where('uc.user_id = ?', answer.user_id)
                  .where('q.id = ?', answer.question_id))
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.user);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/* Edit answer - modify
 * @params {number*} params.answer
 * @params {number} params.answer.user_id
 * @params {number} params.answer.question_id
 * @params {number} params.answer.answer_idx
 * @params {string} params.answer.answer_text
 * @params {number} params.answer.matching_point_1 - 후보 ID 1 의 매칭 점수 
 * @params {number} params.answer.matching_point_2 - 후보 ID 2 의 매칭 점수 
 * @params {number} params.answer.matching_point_3 - 후보 ID 3 의 매칭 점수 
 * @params {number} params.answer.matching_point_4 - 후보 ID 4 의 매칭 점수 
 * @params {number} params.answer.matching_point_5 - 후보 ID 5 의 매칭 점수
 * @params {number} params.answer.matching_point_6 - 후보 ID 6 의 매칭 점수 
 * @params {number} params.answer.matching_point_7 - 후보 ID 7 의 매칭 점수
 * @params {number} params.answer.matching_point_8 - 후보 ID 8 의 매칭 점수 
 * @params {fn}	  	callback(err, result) - result : info
 */
AnswerModel.prototype.update = function(params, callback) {

	// Validation
	var answer = params.answer;
	var validMsg = validateAnswer(answer);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table(`nudep${this.version}.user_answer`)
              .setFields(answer)
              .set('modified','NOW()',{dontQuote:true})
              .where('user_id = ?', answer.user_id)
              .where('question_id = ?', answer.question_id);
  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }
  
  // Query
  base.update(c, sql.toString(), callback);

}


/* Answers
 * @params {number} params.id
 * @params {number} params.user_id
 * @params {number} params.question_id
 * @params {fn}     callback(err, result);
 */
AnswerModel.prototype.answer = function(params, callback, master) {
  // Validation
  console.log(params);

  var c = new Client(master ? base.db_config : base.read_only);

  try {
    var sql = squel.select()
    					.field('ua.id')
              .field('ua.user_id')
              .field('ua.question_id')
              .field('ua.answer_text')
              .field('ua.answer_idx')
              .field('ua.matching_point_1')
              .field('ua.matching_point_2')
              .field('ua.matching_point_3')
              .field('ua.matching_point_4')
              .field('ua.matching_point_5')
              .field('ua.matching_point_6')
              .field('ua.matching_point_7')
              .field('ua.matching_point_8')
              .field('ua.created')
              .field('ua.modified')
              .from(`nudep${this.version}.user_answer`,'ua');

    if (params.question_id)
      sql = sql.where('question_id = ?', params.question_id);

  	if (params.user_id)
  		sql = sql.where('user_id = ?', params.user_id);

  	if (params.id)
  		sql = sql.where('id = ?', params.id);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, params.simple ? answerObjectMap(result[0]) : result[0]);
  });

}





/* Answers
 * @params {number} 	params.user_id
 * @params {boolean}  params.common - 1, 0
 * @params {number}   params.category_id
 * @params {number}   params.answer_text
 * @params {fn}       callback(err, result);
 */
AnswerModel.prototype.answers = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  console.log(params, 'ANSWER PARAMS');

  try {
    var sql = squel.select()
              .field('q.category_id')
              .field('q.id','question_id')
              .field('ua.answer_idx')
              .field('q.answers')
              .from(`nudep${this.version}.user_answer`,'ua')
              .left_join(`nudep${this.version}.question`,'q','q.id=ua.question_id')
              .order('q.category_id');

    if (params.common != undefined)
      sql = sql.where('q.common = ?', params.common);

    if (params.category_id)
      sql = sql.where('q.category_id = ?', params.category_id);

  	if (params.user_id)
  		sql = sql.where('user_id = ?', params.user_id);

  	if (params.answer_text)
  		sql = sql.where('answer_text LIKE ?', params.answer_text);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}


/* Delete a user answers
 * @param   {Object}  params
 * @param   {number}  params.user_id
 * @param   {number}  params.common - 0, 1
 * @param   {fn}      callback(err, result)
 */
AnswerModel.prototype.deleteUserAnswers = function(params, callback) {
  // Validation
  if (params.user_id == undefined) 
    return callback({error: 'lack of parameter: user_id'}, null);

  var c = new Client(base.db_config);

  var sql = squel.delete()
            .from(`nudep${this.version}.user_answer`)
            .where('user_id = ?', params.user_id);

  if (params.common != undefined) {
    sql = `DELETE a FROM nudep${this.version}.user_answer a LEFT JOIN nudep${this.version}.question q ON q.id=a.question_id WHERE a.user_id=${params.user_id} AND q.common=${params.common}`;
  }

  console.log(sql.toString());
  var result = false;
  var error = null;

  c.query(sql.toString(), function(err, rows) {
    if (err)
      error = err;
    else {
      result = rows.info;
    }
  });

  c.on('end', function() {
    console.log('deleted user answer : ', result);
    callback(error, result);
  });

  c.end();
}

AnswerModel.prototype.deleteUserAnswers


module.exports = AnswerModel;