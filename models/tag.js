const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var TagModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};




function validateUserTag(user_tag) {
	// Validation
	var error;
	if (!user_tag) 
		return {error: 'lack of parameter : user_tag'};

	if (user_tag.user_id == undefined) 
		return {error: 'lack of parameter : user_id'};

  if (user_tag.tag_id == undefined) 
    return {error: 'lack of parameter : tag_id'};


	return true;
}


/* Create a user tag
 * @params {number*} params.tag
 * @params {number} params.tag.tag_id
 * @params {number} params.tag.user_id
 * @params {fn}	  	callback(err, result) - result : insertId
 */
TagModel.prototype.createUserTag = function(params, callback) {

	// Validation
	var tag = params.tag;
	
	var validMsg = validateUserTag(tag);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

	var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`nudep${this.version}.user_tag`)
              .setFields(tag)
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/* Update user tag 
 * @params {number*} params.user_tag
 * @params {number} params.user_tag.tag_id
 * @params {number} params.user_tag.user_id
 * @params {number} params.user_tag.selected_promise
 * @params {number} params.user_tag.matching_point_1
 * @params {number} params.user_tag.matching_point_2
 * @params {number} params.user_tag.matching_point_3
 * @params {number} params.user_tag.matching_point_4
 * @params {number} params.user_tag.matching_point_5
 * @params {number} params.user_tag.matching_point_6
 * @params {fn}	  	callback(err, result) - result : info
 */

TagModel.prototype.updateUserTag = function(params, callback) {

	// Validation
	var user_tag = params.user_tag;
	var validMsg = validateUserTag(user_tag);
	if (validMsg != true) {
		return callback(validMsg, null);
	}

  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table(`nudep${this.version}.user_tag`)
              .setFields(user_tag)
              .where('user_id = ?', user_tag.user_id)
              .where('tag_id = ?', user_tag.tag_id);
  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }
  
  // Query
  base.update(c, sql.toString(), callback);

}



/* Tag
 * @params {number} params.id
 * @params {fn}     callback(err, result);
 */
TagModel.prototype.tag = function(params, callback) {
  // Validation
  console.log(params);

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
    					.field('ut.id')
              .field('ut.tag_id')
              .field('c.id', 'category_id')
              .field('c.name', 'category_name')
              .field('t.name')
              .from(`nudep${this.version}.user_tag`,'ut')
              .left_join(`nudep${this.version}.tag`,'t','ut.tag_id=t.id')
              .left_join(`nudep${this.version}.category`,'c','c.id=t.category_id');

    if (params.tag_id)
      sql = sql.where('ut.tag_id = ?', params.tag_id);

  	if (params.user_id)
  		sql = sql.where('ut.user_id = ?', params.user_id);

  	if (params.id)
  		sql = sql.where('ut.id = ?', params.id);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result[0]);
  });

}





/* Tags
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.tag_id
 * @params {array}    params.inIDs - ids array
 * @params {fn}       callback(err, result);
 */
TagModel.prototype.tags = function(params, callback, master) {
  // Validation

  var c = new Client(master ? base.db_config : base.read_only);

  try {
    var sql = squel.select()
              .field('t.id')
              .field('t.name')
              .field('t.category_id')
              .from(`nudep${this.version}.tag`,'t');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.inIDs)
      sql = sql.where('id IN ?', params.inIDs);

    if (params.user_id != undefined)
      sql = sql.left_join('user_tag','ut','ut.tag_id=t.id')
            .where('ut.user_id = ?', params.user_id)


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}


/* Tags
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.promise_id
 * @params {fn}       callback(err, result);
 */
TagModel.prototype.userTags = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('ut.id')
              .field('ut.tag_id')
              .from(`nudep${this.version}.user_tag`,'ut');

    if (params.id)
      sql = sql.where('ut.id = ?', params.id);

    if (params.user_id)
      sql = sql.where('ut.user_id = ?', params.user_id);

    if (params.promise_id) {
      sql = sql.left_join(`nudep${this.version}.promise`,'p','p.tag_id=ut.tag_id')
            .where('q.id = ?', params.promise_id);
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



/* Delete a user tag
 * @param   {Object}  params
 * @param   {number}  params.user_id
 * @param   {fn}      callback(err, result)
 */
TagModel.prototype.deleteUserTag = function(params, callback) {
  // Validation
  if (params.user_id == undefined) 
    return callback({error: 'lack of parameter: user_id'}, null);

  var c = new Client(base.db_config);

  var sql = squel.delete()
            .from(`nudep${this.version}.user_tag`)
            .where('user_id = ?', params.user_id)
            .toString();

  console.log(sql);
  var result = false;
  var error = null;

  c.query(sql, function(err, rows) {
    if (err)
      error = err;
    else {
      result = rows.info;
    }
  });

  c.on('end', function() {
    console.log('delete user tag : ', result);
    callback(error, result);
  });

  c.end();
}


/* top tags 3
 * @params {string} params.gender
 * @params {string} params.age
 * @params {string} params.job
 * @params {fn}	  	callback(err, result) - result : insertId
 */
TagModel.prototype.tagsTop3 = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('t.name')
            .field('ut.tag_id')
            .field('count(*)', 'count')
            .from(`nudep${this.version}.user_tag`,'ut')
            .left_join(`nudep${this.version}.tag`,'t','ut.tag_id=t.id')
            .left_join(`nudep.user`,'u','ut.user_id=u.id')
            .group('ut.tag_id')
            .order('count',false)
            .limit(3);

  if (params.gender) sql = sql.where('u.gender = ?', params.gender);
  if (params.age) sql = sql.where('u.age = ?', params.age);
  if (params.job) sql = sql.where('u.job = ?', params.job);

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });
}


/* tag matrix
 * @params {array} params.columns
 * @params {fn}     callback(err, result) - result : insertId
 */
TagModel.prototype.matrix = function(params, callback) {

  if (!params.columns || !Array.isArray(params.columns)) return callback({error: 'lack of parameter: columns'}, null);

  var c = new Client(base.read_only);

  var sql = squel.select({autoQuoteFieldNames: false})
            .field('t.id','id')
            .field('m.tag', 'name')
            .field('t.category_id')
            .field(`(${params.columns.join('+')})`, 'weight')
            .from(`nudep${this.version}.tmp_recomm_matrix`,'m')
            .left_join(`nudep${this.version}.tag`,'t','t.name=m.tag')
            .order('weight', false);

  console.log(sql.toString());

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });
}



module.exports = TagModel;
