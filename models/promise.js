const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var PromiseModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};




function validateUserPromoise(obj) {
  // Validation
  var error;
  if (!obj) 
    return {error: 'lack of parameter : obj'};

  if (obj.user_id == undefined) 
    return {error: 'lack of parameter : user_id'};

  if (obj.promise_id == undefined) 
    return {error: 'lack of parameter : promise_id'};

  return true;
}


function userPromiseObjectMap(a) {
  var r = {};
  r.id = a.id;
  r.promise = {
    id: a.promise_id,
    title: a.promise_title,
    tag_id: a.tag_id
  };
  r.created = a.created;

  return r;
}


/* Create a promise
 * @params {number*} params.promise
 * @params {number} params.promise.promise_id
 * @params {number} params.promise.user_id
 * @params {fn}     callback(err, result) - result : insertId
 */
PromiseModel.prototype.createUserPromise = function(params, callback) {

  // Validation
  var promise = params.promise;
  
  var validMsg = validateUserPromoise(promise);
  if (validMsg != true) {
    return callback(validMsg, null);
  }

  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`nudep${this.version}.user_promise`)
              .setFields(promise)
              .set('user_tag_id',
                squel.select()
                .field('max(ut.id)','id')
                .from(`nudep${this.version}.user_tag`,'ut')
                .left_join(`nudep${this.version}.promise`,'p','p.tag_id=ut.tag_id')
                .where('p.id = ?', promise.promise_id))
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}




/* Promoise
 * @params {number}  params.id
 * @params {fn}      callback(err, result);
 */
PromiseModel.prototype.promise = function(params, callback) {
  // Validation
  if (params.id == undefined)
    return callback({error:'lack of parameters: id'});

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('p.id')
              .field('p.candidate_id')
              .field('p.tag_id')
              .field('p.title')
              .from(`nudep${this.version}.promise`,'p')
              .where('p.id = ?', params.id);


    if (params.order && params.order.by && params.asc != undefined) {
      sql = sql.order(params.order.by, params.order.asc);
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result[0]);
  });

}


/* Promoises
 * @params {number}  params.id
 * @params {object}   params.order
 * @params {string}   params.order.by
 * @params {boolean}  params.order.asc
 * @params {array}    params.excludes - ids array
 * @params {fn}      callback(err, result);
 */
PromiseModel.prototype.promises = function(params, callback) {

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from(`nudep${this.version}.promise`,'p');


    if (params.order && params.order.by && params.asc != undefined) {
      sql = sql.order(params.order.by, params.order.asc);
    }

    if (params.excludes && Array.isArray(params.excludes)) {
      sql = sql.where('candidate_id NOT IN ?', params.excludes);
    }



  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}




/* Promoise
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.promise_id
 * @params {fn}       callback(err, result);
 */
PromiseModel.prototype.userPromise = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('up.id')
              .field('p.id', 'promise_id')
              .field('p.title', 'promise_title')
              .field('p.tag_id', 'tag_id')
              .field('up.created')
              .from(`nudep${this.version}.user_promise`,'up')
              .left_join(`nudep${this.version}.promise`,'p','p.id=up.promise_id')
              ;

    if (params.id)
      sql = sql.where('up.id = ?', params.id);

    if (params.user_id)
      sql = sql.where('up.user_id = ?', params.user_id);

    if (params.promise_id)
      sql = sql.where('up.promise_id = ?', params.promise_id);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, (result[0] ? userPromiseObjectMap(result[0]) : undefined));
  });

}

/* User Promoises
 * @params {number}   params.promise_id
 * @params {number}   params.user_id
 * @params {number}   params.tag_id
 * @params {fn}       callback(err, result);
 */
PromiseModel.prototype.userPromises = function(params, callback, master) {
  // Validation

  var c = new Client(master ? base.db_config : base.read_only);

  try {
    var sql = squel.select()
              .field('p.id', 'id')
              .field('p.title', 'title')
              .field('p.tag_id', 'tag_id')
              .from(`nudep${this.version}.user_tag`,'ut')
              .left_join(`nudep${this.version}.promise`,'p','p.id=ut.selected_promise')
              .where('ut.selected_promise is not null')
              ;

    if (params.user_id)
      sql = sql.where('ut.user_id = ?', params.user_id);

    if (params.tag_id)
      sql = sql.where('ut.tag_id = ?', params.tag_id);

    if (params.promise_id)
      sql = sql.where('p.id = ?', params.promise_id);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



/* Delete a user promise
 * @param   {Object}  params
 * @param   {number}  params.user_id
 * @param   {fn}      callback(err, result)
 */
PromiseModel.prototype.deleteUserPromiseByTag = function(params, callback) {
  // Validation
  if (params.user_id == undefined) 
    return callback({error: 'lack of parameter: user_id'}, null);

  var c = new Client(base.db_config);

  var sql = squel.delete()
            .from(`nudep${this.version}.user_promise`)
            .where('user_id = ?', params.user_id)
            .where('user_tag_id IN ?', squel.select().field('id').from('user_tag').where('user_id = ?', params.user_id))
            .toString();

  console.log(sql);
  var result = false;
  var error = null;

  c.query(sql, function(err, rows) {
    if (err)
      error = err;
    else {
      result = rows.info;
    }
  });

  c.on('end', function() {
    console.log('delete user tag : ', result);
    callback(error, result);
  });

  c.end();
}


// /* Delete a user promise
//  * @param   {Object}  params
//  * @param   {number}  params.user_id
//  * @param   {fn}      callback(err, result)
//  */
// PromiseModel.prototype.deleteUserPromise = function(params, callback) {
//   // Validation
//   if (params.user_id == undefined) 
//     return callback({error: 'lack of parameter: user_id'}, null);

//   var c = new Client(base.db_config);

//   var sql = squel.delete()
//             .from(`nudep${this.version}.user_promise`)
//             .where('user_id = ?', params.user_id)
//             .toString();

//   console.log(sql);
//   var result = false;
//   var error = null;

//   c.query(sql, function(err, rows) {
//     if (err)
//       error = err;
//     else {
//       result = rows.info;
//     }
//   });

//   c.on('end', function() {
//     console.log('delete user tag : ', result);
//     callback(error, result);
//   });

//   c.end();
// }


module.exports = PromiseModel;