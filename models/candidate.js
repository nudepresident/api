const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');





var CandidateModel = function() {};


/* Candidates
 * @params {number}   params.id
 * @params {number}   params.name
 * @params {array}    params.excludes - ids array
 * @params {fn}       callback(err, result);
 */
CandidateModel.prototype.candidates = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('nudep.candidate','c');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.name)
      sql = sql.where('name LIKE ?', params.name);

    if (params.excludes && Array.isArray(params.excludes)) {
      sql = sql.where('id NOT IN ?', params.excludes);
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



module.exports = CandidateModel;