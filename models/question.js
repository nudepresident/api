const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var QuestionModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};



 function formattedQuestionObjects(qs) {
  if (!qs || qs.length < 1) return [];

  for (var i in qs) {
    var r = qs[i];
    r.answers_by_candidate = {
      // '1': r.c_answer_1,
      '2': r.c_answer_2,
      '3': r.c_answer_3,
      // '4': r.c_answer_4,
      '5': r.c_answer_5,
      // '6': r.c_answer_6,
      '7': r.c_answer_7,
      '8': r.c_answer_8
    };

    // delete r.c_answer_1;
    delete r.c_answer_2;
    delete r.c_answer_3;
    // delete r.c_answer_4;
    delete r.c_answer_5;
    // delete r.c_answer_6;
    delete r.c_answer_7;
    delete r.c_answer_8;

    delete r.description;
  }

  return qs;

 }




/* Questions
 * @params {number}   params.id
 * @params {number}   params.user_id
 * @params {number}   params.category_id
 * @params {number}   params.common - 0, 1
 * @params {boolean}  params.formatted
 * @params {fn}       callback(err, result);
 */
QuestionModel.prototype.questions = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from(`nudep${this.version}.question`,'q');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.category_id)
      sql = sql.where('category_id = ?', params.category_id);

    if (params.common === 0 || params.common === 1)
      sql = sql.where('common = ?', params.common);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, params.formatted ? formattedQuestionObjects(result) : result);
  });

}

module.exports = QuestionModel;