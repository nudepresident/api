const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');


var UserModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};


function userObjectMap(user) {

  if (!user) return null;

  var r = {};
  r.id = user.id;
  r.name = user.name;
  r.location_id = user.location_id;
  r.age = user.age;
  r.gender = user.gender;
  r.job = user.job;
  r.married = user.married;
  r.has_children = user.has_children;
  r.joined = user.joined;
  r.profileimage = user.profileimage;

  // Facebook
  if (user.facebook_id) {
    r.facebook = {
      id: user.facebook_id,
      profileimage: user.profileimage
    }
  }

  r.data = {
    current_candidate: user.current_candidate,
    final_candidate: user.final_candidate,
    favorite_promise: user.favorite_promise,
    dislike_promise: user.dislike_promise,
    message: user.message,
    final_reason: user.final_reason,
    sent_message: user.sent_message
  };

  return r;
}

/* User */

/* Get a user
 * @param 	{Object}	params
 * @param   {boolean}  params.simple - For formatting
 * @param 	{number}	params.id
 * @param   {string}  params.email
 * @param   {number}  params.facebook_id
 * @param   {string}  params.auth.email
 * @param   {string}  params.auth.password
 * @param   {Object}  params.auth.facebook - facebook identifier
 * @param   {Object}  params.auth.naver - naver identifier
 * @param 	{fn}			callback(err, result)
 */
//INSERT INTO t VALUES (AES_ENCRYPT('text',SHA2('password',512)));
UserModel.prototype.user = function(params, callback, master) {

	var c = new Client(master ? base.db_config : base.read_only);

  try {

    var sql = squel.select()
              .field('u.id', 'id')
              .field('u.name')
              .field('u.facebook_id')
              .field('u.email')
              .field('u.profileimage')
              .field('u.birthday')
              .field('u.age')
              .field('u.gender')
              .field('u.location_id')
              .field('u.job')
              .field('u.married')
              .field('u.has_children')
              .field('u.last_login')
              .field('u.joined')
              .field('ut.current_candidate')
              .field('ut.final_candidate')
              .field('ut.favorite_promise')
              .field('ut.dislike_promise')
              .field('ut.message')
              .field('ut.final_reason')
              .field('ut.modified')
              .field('ut.sent_message')
              .from('nudep.user','u')
              .left_join(`nudep${this.version}.user_test`, 'ut', 'ut.user_id=u.id')
              .limit(1);


    if (params.id !== undefined) {
      sql = sql.where('u.id = ?', params.id);
    }
    
    else if (params.email !== undefined) {
      sql = sql.where('email like ?', params.email);
    }

    else if (params.facebook_id != undefined) {
      sql = sql.where('facebook_id = ?', params.facebook_id);
    }

    else if (params.auth) {

      sql = sql
            

      if (params.auth.email !== undefined && params.auth.password !== undefined) {
        sql = sql
              .field('ua.identifier', 'auth_email')
              .left_join(squel.select().field('user_id').field('identifier').field('credential').from('nudep.user_auth').where('type LIKE ?','EMAIL'), 'ua', 'u.id = ua.user_id')
              .where('ua.credential LIKE SHA2(?,256)', params.auth.password)
              .where('ua.identifier LIKE ?', params.auth.email);
      }

      else if (params.auth.facebook !== undefined) {
        sql = sql
              .left_join(squel.select().field('user_id').field('identifier').field('credential').from('nudep.user_auth').where('type LIKE ?','FACEBOOK'), 'ua1', 'u.id = ua1.user_id')
              .field('ua1.identifier', 'facebook')
              .where('ua1.identifier LIKE ?', params.auth.facebook); 
      }

      else if (params.auth.naver !== undefined) {
        sql = sql
              .left_join(squel.select().field('user_id').field('identifier').field('credential').from('nudep.user_auth').where('type LIKE ?','NAVER'), 'ua2', 'u.id = ua2.user_id')
              .field('ua2.identifier', 'naver')
              .where('ua2.identifier LIKE ?', params.auth.naver); 
      } 

      else {
        return callback({error: 'query condition is not available'}, null);
      }

    }

    else {
      return callback({error: 'query condition is not available'}, null);
    }


  } catch (e) {
    console.log(e);
  }

  base.get(c, sql.toString(), function(error, result){
    var r = result[0] ? (params.simple ? userObjectMap(result[0]) : result[0]) : result[0];
    callback(error, r);
  });
}


/* Get a user id
 * @param   {Object}  params
 * @param   {Object}  params.facebook_id - facebook identifier
 * @param   {Object}  params.naver_id - naver identifier
 * @param   {Object}  params.email
 */
UserModel.prototype.userId = function(params, callback, master) {
  var c = new Client(master ? base.db_config : base.read_only);

  try {

    var sql = squel.select()
              .field('u.id', 'id')
              .from('nudep.user', 'u')
              .limit(1);


    if (params.facebook_id !== undefined) {
        sql = sql
              .left_join('nudep.user_auth','ua','ua.user_id=u.id')
              .where('ua.type LIKE ?','FACEBOOK')
              .where('ua.identifier LIKE ?', params.facebook_id); 
    }

    else if (params.naver_id !== undefined) {
      sql = sql
              .left_join('nudep.user_auth','ua','ua.user_id=u.id')
              .where('ua.type LIKE ?','NAVER')
              .where('ua.identifier LIKE ?', params.naver_id); 
    }

    if (params.email !== undefined) {
      sql = sql.where('email like ?', params.email);
    }

  } catch (e) {
    console.log(e);
  }

  base.get(c, sql.toString(), function(error, result){
    var r = result[0];
    callback(error, r);
  });

}




/* Get list of user
 * @param 	{Object}	params
 * @param 	{number}	params.offset
 * @param 	{number}	params.limit
 * @param   {boolean}  params.has_message
 * @param   {object}  params.order
 * @param   {string}  params.order.by
 * @param   {boolean}  params.order.asc
 * @param   {boolean}  params.location
 * @param   {boolean}  params.with_final_candidate
 * @param 	{fn}			callback(err, result)
 */
UserModel.prototype.users = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('u.id', 'id')
            .field('u.name')
            .field('u.facebook_id')
            .field('u.email')
            .field('u.profileimage')
            .field('u.birthday')
            .field('u.age')
            .field('u.gender')
            .field('u.location_id')
            .field('u.job')
            .field('u.married')
            .field('u.has_children')
            .field('u.last_login')
            .field('u.joined')
            .field('ut.current_candidate')
            .field('ut.final_candidate')
            .field('ut.favorite_promise')
            .field('ut.dislike_promise')
            .field('ut.message')
            .field('ut.final_reason')
            .field('ut.modified')
            .field('ut.sent_message')
            .from('nudep.user','u')
            .left_join(`nudep${this.version}.user_test`, 'ut', 'ut.user_id=u.id');

  if (params.offset !== undefined) {
    sql = sql.offset(params.offset);
  }

  if (params.limit !== undefined) {
    sql = sql.limit(params.limit);
  }

  if (params.has_message) {
    sql = sql.where('ut.message IS NOT NULL')
          .where('char_length(ut.message) > 2');
  }

  if (params.order != undefined && params.order.by && params.order.asc != undefined) {
    sql = sql.order(params.order.by, params.order.asc);
  }

  if (params.location != undefined) {
    sql = sql.field('l.district')
          .left_join('nudep.location','l','l.id=u.location_id');
  }

  if (params.with_final_candidate) {
    sql = sql.field('c.name','candidate')
          .left_join('nudep.candidate','c','c.id=ut.final_candidate');
  }

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result);
  });
}


/* Create a user
 * @param 	{Object}	params
 * @param   {string}  params.user.name
 * @param   {string}  params.user.email
 * @param 	{fn}			callback(err, result) - result : insertId
 */
UserModel.prototype.create = function(params, callback) {

	// Validation
	if (params.user == undefined) 
		return callback({error: 'lack of parameter : user'}, null);

  console.log(params.user);
	var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into("nudep.user")
              .setFields(params.user)
              .set('joined','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.user);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}

/* Edit user - modify
 * @param 	{Object}	params
 * @param 	{Object}	params.user
 * @param   {number*}  params.user.id
 * @param   {array}   params.null_fields
 * @param 	{fn}			callback(err, result)
 */
UserModel.prototype.update = function(params, callback) {

  var c = new Client(base.db_config);

  // Validation
  if (params.user.id == undefined) 
    return callback({error: 'lack of parameter : id'}, null);

  var sent_message = (params.user.message != undefined);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table("nudep.user")
              .setFields(params.user)
              .set('modified','NOW()',{dontQuote:true})
              .where('id = ?', params.user.id)
              ;

    if (params.null_fields && params.null_fields.length > 0) {
      for (var i in params.null_fields) {
        sql = sql.set(params.null_fields[i], 'NULL', {dontQuote:true});
      }
    }

    if (sent_message) {
      sql = sql.set('sent_message','NOW()',{dontQuote:true})
    }
  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }

  
  // Query
  base.update(c, sql.toString(), callback);

}









/* Update a user test
 * @param   {Object}  params
 * @param   {string}  params.test.current_candidate
 * @param   {string}  params.test.final_candidate
 * @param   {string}  params.test.favorite_promise
 * @param   {string}  params.test.dislike_promise
 * @param   {string}  params.test.message
 * @param   {string}  params.test.final_reason
 * @param   {array}   params.null_fields
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserModel.prototype.updateTest = function(params, callback) {

  // Validation
  if (params.test == undefined) 
    return callback({error: 'lack of parameter : test'}, null);

  console.log(params.test);
  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table(`nudep${this.version}.user_test`)
              .setFields(params.test)
              .set('modified','NOW()',{dontQuote:true})
              .where('user_id = ?', params.test.user_id)
              ;

    if (params.null_fields && params.null_fields.length > 0) {
      for (var i in params.null_fields) {
        sql = sql.set(params.null_fields[i], 'NULL', {dontQuote:true});
      }
    }

    if (params.sent_message) {
      sql = sql.set('sent_message','NOW()',{dontQuote:true})
    }

  } catch (e) {
    console.log(params.test);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}



/* Create a user test
 * @param   {Object}  params
 * @param   {string}  params.test.current_candidate
 * @param   {string}  params.test.final_candidate
 * @param   {string}  params.test.favorite_promise
 * @param   {string}  params.test.dislike_promise
 * @param   {string}  params.test.message
 * @param   {string}  params.test.final_reason
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserModel.prototype.createTest = function(params, callback) {

  // Validation
  if (params.test == undefined) 
    return callback({error: 'lack of parameter : test'}, null);

  console.log(params.test);
  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`nudep${this.version}.user_test`)
              .setFields(params.test)
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.user);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/* Get user test
 * @param   {number}  params.user_id
 */
UserModel.prototype.test = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('*')
            .from(`nudep${this.version}.user_test`)
            .where('user_id = ?', params.user_id);

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result[0]);
  });
}





// USER AUTH

/* Get user auth
 * @param   {number}  params.auth.user_id
 * @param   {string}  params.auth.type - EMAIL, FACEBOOK, NAVER
 * @param   {string}  params.auth.identifier
 */
UserModel.prototype.auth = function(params, callback) {
  // Validation
  if (params.auth == undefined) 
    return callback({error: 'lack of parameter : auth'}, null);


  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('ua.*')
              .from('nudep.user_auth','ua')
              .limit(1);

    if (params.auth.user_id)
      sql = sql.where('user_id = ?', params.auth.user_id);

    if (params.auth.type)
      sql = sql.where('type LIKE ?', params.auth.type);

    if (params.auth.identifier)
      sql = sql.where('identifier = ?', params.auth.identifier);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result[0]);
  });


}

/* Create a user auth
 * @param   {Object}  params
 * @param   {Object}  params.auth
 * @param   {number}  params.auth.user_id
 * @param   {string*}  params.auth.type - EMAIL, FACEBOOK, NAVER
 * @param   {string}  params.auth.identifier
 * @param   {string}  params.auth.credential_type - PASSWORD, TOKEN
 * @param   {string}  params.auth.credential
 * @param   {number}  params.auth.expires
 * @param   {string}  params.auth.raw
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserModel.prototype.createAuth = function(params, callback) {

  // Validation
  if (params.auth == undefined) 
    return callback({error: 'lack of parameter : auth'}, null);

  var cred = params.auth.credential;
  delete params.auth.credential;

  console.log(params.auth);
  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into("nudep.user_auth")
              .setFields(params.auth)
              .set('credential', "SHA2('"+cred+"',256)", {dontQuote:true})
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.auth);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/* Update a user auth
 * @param   {Object}  params
 * @param   {Object}  params.auth
 * @param   {number}  params.auth.user_id
 * @param   {string*}  params.auth.type - EMAIL, FACEBOOK, NAVER
 * @param   {string}  params.auth.identifier
 * @param   {string}  params.auth.credential_type - PASSWORD, TOKEN
 * @param   {string}  params.auth.credential
 * @param   {number}  params.auth.expires
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserModel.prototype.updateAuth = function(params, callback) {

  // Validation
  if (params.auth == undefined) 
    return callback({error: 'lack of parameter : auth'}, null);

  var cred = params.auth.credential;
  delete params.auth.credential;

  console.log(params.auth);
  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table("nudep.user_auth")
              .setFields(params.auth)
              .set('modified','NOW()',{dontQuote:true})
              .where('user_id = ?', params.auth.user_id)
              .where('type like ?', params.auth.type)
              ;

    if (params.auth.type == 'EMAIL') {
      sql = sql.set('credential', "SHA2('"+cred+"',256)", {dontQuote:true});
    } else {
      sql = sql.set('credential', cred);
    }
  } catch (e) {
    console.log(params.auth);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.update(c, sql.toString(), callback);
}



/* Delete a user
 * @param   {Object}  params
 * @param   {number}  params.id 
 * @param   {fn}      callback(err, result)
 */
UserModel.prototype.delete = function(params, callback) {
  // Validation
  if (params.id == undefined) 
    return callback({error: 'lack of parameter: id'}, null);

  var c = new Client(base.db_config);

  var sql = squel.delete()
            .from('nudep.user')
            .where('id = ?', params.id)
            .toString();

  console.log(sql);
  var result = false;
  var error = null;

  c.query(sql, function(err, rows) {
    if (err)
      error = err;
    else {
      result = rows.info;
    }
  });

  c.on('end', function() {
    console.log('delete user : ', result);
    callback(error, result);
  });

  c.end();
}


/* Get total count of users
 * @param   {Object}  params
 * @param   {Boolean}  params.sent_message
 * @param   {fn}      callback(err, result)
 */
UserModel.prototype.total = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('count(*)','total')
            .from('nudep.user','u');

  if (params.sent_message)
    sql.where('sent_message IS NOT NULL');

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result[0]);
  });
}

UserModel.prototype.countByAge = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('age')
            .field('count(*)','count')
            .where('age IS NOT NULL')
            .where('age != "0"')
            .from('nudep.user','u')
            .group('age');

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result);
  });
}

UserModel.prototype.countByGender = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('gender')
            .field('count(*)','count')
            .where('gender IS NOT NULL')
            .where('gender != ""')
            .from('nudep.user','u')
            .group('gender');

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result);
  });
}

UserModel.prototype.countByJob = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('job')
            .field('count(*)','count')
            .where('job IS NOT NULL')
            .from('nudep.user','u')
            .order('count', false)
            .group('job');

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result);
  });
}


UserModel.prototype.messageTotal = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = `select sum(cnt) total
              from (
              select count(*) cnt from nudep.user
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v1.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v2.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v3.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v4.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v5.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v6.user_test
              where char_length(message) > 2
              union
              select count(*) cnt from nudep_v7.user_test
              where char_length(message) > 2) a`;

  // Query
  base.get(c, sql, function(error, result) {
    callback(error, result[0]);
  });
}


module.exports = UserModel;
