const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var ResultModel = function(version) {
  this.version = version != undefined ? `_v${version}` : '';
};


/* Answer Points Summary
 * @params {object} params
 * @params {number} params.user_id
 * @params {boolean} params.group_by_category
 * @params {boolean} params.common
 * @params {fn}     callback(err, result) - result 
 */
ResultModel.prototype.answerPointsSummary = function(params, callback) {
  // Validation
  console.log(params);

  /*
  SELECT 
      SUM(ua.matching_point_1) AS '질문 남경필',
      SUM(ua.matching_point_2) AS '질문 문재인',
      SUM(ua.matching_point_3) AS '질문 안철수',
      SUM(ua.matching_point_4) AS '질문 안희정',
      SUM(ua.matching_point_5) AS '질문 유승민',
      SUM(ua.matching_point_6) AS '질문 이재명',
      COUNT(ua.id),
      uc.category_id
  FROM
      user u
          LEFT JOIN
      user_answer ua ON ua.user_id = u.id
          LEFT JOIN
      question q ON q.id = ua.question_id
          LEFT JOIN
      user_category uc ON uc.user_id = u.id
          AND uc.category_id = q.category_id
  WHERE
      u.id = 1
  GROUP BY uc.category_id
  */

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('IFNULL(SUM(ua.matching_point_1),0)', 'sum_1')
              .field('IFNULL(SUM(ua.matching_point_2),0)', 'sum_2')
              .field('IFNULL(SUM(ua.matching_point_3),0)', 'sum_3')
              .field('IFNULL(SUM(ua.matching_point_4),0)', 'sum_4')
              .field('IFNULL(SUM(ua.matching_point_5),0)', 'sum_5')
              .field('IFNULL(SUM(ua.matching_point_6),0)', 'sum_6')
              .field('IFNULL(SUM(ua.matching_point_7),0)', 'sum_7')
              .field('IFNULL(SUM(ua.matching_point_8),0)', 'sum_8')
              .field('COUNT(ua.id)', 'count')
              .from(`nudep.user`,'u')
              .left_join(`nudep${this.version}.user_answer`,'ua','ua.user_id=u.id')
              .left_join(`nudep${this.version}.question`,'q','q.id=ua.question_id');

    if (params.user_id)
      sql = sql.where('u.id = ?', params.user_id);

    if (params.group_by_category)
      sql = sql.field('q.category_id')
            .where('q.category_id is not null')
            .group('q.category_id');

    if (params.common != undefined)
      sql = sql.where('q.common = ?', params.common);


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });
};


/* Promise Points Summary
 * @params {object} params
 * @params {number} params.user_id
 * @params {boolean} params.group_by_category
 * @params {fn}     callback(err, result) - result 
 */
ResultModel.prototype.promisePointsSummary = function(params, callback) {
  // Validation
  console.log(params);

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('IFNULL(SUM(ut.matching_point_1),0)', 'sum_1')
              .field('IFNULL(SUM(ut.matching_point_2),0)', 'sum_2')
              .field('IFNULL(SUM(ut.matching_point_3),0)', 'sum_3')
              .field('IFNULL(SUM(ut.matching_point_4),0)', 'sum_4')
              .field('IFNULL(SUM(ut.matching_point_5),0)', 'sum_5')
              .field('IFNULL(SUM(ut.matching_point_6),0)', 'sum_6')
              .field('IFNULL(SUM(ut.matching_point_7),0)', 'sum_7')
              .field('IFNULL(SUM(ut.matching_point_8),0)', 'sum_8')
              .field('COUNT(ut.id)', 'count')
              .from(`nudep.user`,'u')
              .left_join(`nudep${this.version}.user_tag`,'ut','ut.user_id=u.id');

    if (params.user_id)
      sql = sql.where('u.id = ?', params.user_id);

    if (params.group_by_category)
      sql = sql.field('uc.category_id')
            .left_join(`nudep${this.version}.tag`,'t','t.id=ut.tag_id')
            .left_join(`nudep${this.version}.user_category`,'uc','uc.user_id=u.id AND uc.category_id = t.category_id')
            .where('uc.category_id is not null')
            .group('uc.category_id');


  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });
};


module.exports = ResultModel;