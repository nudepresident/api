const WRITE_DB = 'production' == process.env.NODE_ENV 
                  ? '127.0.0.1' //'robin.cnrgtlezngrl.ap-northeast-2.rds.amazonaws.com' 
                  : 'development' == process.env.NODE_ENV ? '13.124.72.106' : '127.0.0.1';

const READ_DB = 'production' == process.env.NODE_ENV 
                  ? '127.0.0.1' // 'robin.cnrgtlezngrl.ap-northeast-2.rds.amazonaws.com'
                  : 'development' == process.env.NODE_ENV ? '13.124.72.106' : '127.0.0.1';


module.exports = {
  db_config : {
    host: WRITE_DB,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    db: 'nudep',
    charset: 'utf8'
  },
  read_only : {
    host: READ_DB, 
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    db: 'nudep',
    charset: 'utf8'
  },
  create: function(c, sql, cb) {
    var result = false;
    var error = null;

    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows.info ? rows.info.insertId : null;  
      }
      
    });

    c.on('end', function() {
      if (error) {
        console.error(error, sql);
      }

      setTimeout(function(){ // Put a interval for sync to replicas
        if (cb)
          cb(error, result);
      },50);
    });

    c.end();
  },
  update: function(c, sql, cb) {
    var result = false;
    var error = null;
    console.log(sql);
    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows.info;  
      }
    });

    c.on('end', function() {
      if (error) {
        console.error(error, sql);
      }

      setTimeout(function(){ // Put a interval for sync to replicas
        if (cb)
          cb(error, result);
      },50);
    });

    c.end();
  },
  get: function(c, sql, cb) {
    var result = false;
    var error = null;
    // console.log(sql);
    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows;
        console.log(result);
      }
    });

    c.on('end', function() {
      cb(error, result);
    });

    c.end();
  }
};
