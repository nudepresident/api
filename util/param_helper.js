module.exports = {
	invalidate: function(params, required_params) {
		for (var i in required_params) {
			var key = required_params[i];
			if (params[key] == undefined) {
				return key;
			}
		}
		return;
	},
	validOneOf: function(params, required_params) {
		var valid = false;
		for (var i in required_params) {
			var key = required_params[i];
			if (params[key] !== undefined) {
				valid = true;
				break;
			}
		}
		return valid;
	}
};