const fs = require('fs');
const Canvas = require('canvas');
const Image = Canvas.Image;
const ChartjsNode = require('chartjs-node');

function readImageFile(src) {
  return new Promise(function (resolve, reject) {
    fs.readFile(src, function(err, bg){
      if (err) return reject(err);
      var img = new Image;
      img.src = bg;
      return resolve(img);
    });
  });
}

function roundCornerImage(img, width, height) {
  const canvas = new Canvas(width, height);
  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, width, height);
  ctx.globalCompositeOperation = 'source-over';
  ctx.drawImage(img, 0, 0, width, height);

  ctx.fillStyle = '#fff';
  ctx.globalCompositeOperation = 'destination-in';
  ctx.beginPath();
  ctx.arc(width/2, width/2, width/2, 0, 2 * Math.PI, true);
  ctx.closePath();
  ctx.fill();

  return canvas;
}

function getRadarChart(labels, data) {
  var chart = new ChartjsNode(280, 245);
  return chart.drawChart({
    type: 'radar',
    data: {
      labels: labels,
      datasets: [
        {
          backgroundColor: 'rgba(80,226,193,0.8)',
          borderColor: '#22b393',
          borderWidth: 1,
          pointBackgroundColor: '#22b393',
          pointBorderColor: '#22b393',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(179,181,198,1)',
          strokeStyle: '#fff',
          data: data,
        },
      ],
    },
    options: {
      scale: {
        gridLines: {
          color: 'rgba(221, 221, 221, .4)',
        },
        angleLines: {
          color: 'rgba(221, 221, 221, .4)',
        },
        ticks: {
          display: false,
          stepSize: 50,
          min: 0,
          max: 150,
        },
        pointLabels: {
          fontColor: '#fff',
          fontSize: 15,
          fontStyle: 'bold',
          fontFamily: 'NanumMyeongjo',
        },
      },
      legend: {
        display: false,
      },
      tooltips: {
        enabled: false,
      },
    },
  }).then(function () {
    // get image as png buffer
    return chart.getImageBuffer('image/png');
  }).then(function (buf) {
    var img = new Image;
    img.src = buf;
    return img;
  });
}

function drawResult(opts) {
  const { ctx, baseX, baseY, rank, candidate, total, categories, labels } = opts;

  return Promise.all([
    readImageFile(__dirname + '/../images/graph/candidate' +
        candidate.id + '.jpg'),
    getRadarChart(
      labels,
      categories.map(function(c) {
        return c * 100 + 50;
      })
    ), 
  ]).then(function(data) {
    ctx.drawImage(
      roundCornerImage(data[0], 40, 40),
      baseX + 67, baseY + 12, 40, 40
    );

    ctx.font = '28px NanumMyeongjo';
    ctx.fillStyle = 'white';
    ctx.fillText(rank + "위", baseX + 117, baseY + 40);

    ctx.font = 'bold 30px NanumMyeongjo';
    ctx.fillStyle = 'white';
    ctx.fillText(candidate.name, baseX + 169, baseY + 40);

    ctx.font = 'bold 32px NanumMyeongjo';
    ctx.fillStyle = '#50e3c2';
    ctx.fillText(parseInt(total * 1000) / 10 + "%", baseX + 259, baseY + 42);

    ctx.drawImage(data[1], baseX + 80, baseY + 70, 250, 225);
    return null;
  });
}

module.exports = {
  readImageFile: readImageFile,
  roundCornerImage: roundCornerImage,
  drawResult: drawResult,
};
