// Session User
// role: manager, writer, guest
// ex) { role: 'manager', user: { id: 123, name: 'xyz'} }

var jwt = require('jwt-simple');
var moment = require('moment');


const secret = 'NudePresident!@#$';
const COOKIE_NAME = 'NUDEPRESIDENT_SESSION';

var default_maxAge = 60*60*24; // 24 hour

const getToken = function(req, maxAge, user) {

  var now = moment().unix();
  var expire = now + (maxAge || default_maxAge);
  var token = {
    date_create: now,
    origin: req.headers.origin,
    ip: req.clientIp,
    ua: req.headers['user-agent'],
    expire: expire,
    user: user
  };

  console.log(token, 'TOKEN CREATED');

  return {
    token: jwt.encode(token, secret),
    expire: expire
  };
};


const getCurrentToken = function(req) {
  var token = req.cookies[COOKIE_NAME];

  if (!token) {
    token = req.params['access_token'] || req.header('x-access-token') || req.header('X-ACCESS-TOKEN') || false;
  }

  if (token)
    return jwt.decode(token, secret);;
  
  return false;
}

const getCurrentPermission = function(req) {
  var token = getCurrentToken(req);
  console.log(token);

  if (token.user && typeof token.user == 'object') {
    return 'LOGGED_IN';
  }
  return 'PUBLIC';
}

const cookieOption = function(maxAge) {
  return {
    maxAge: (maxAge || default_maxAge),
    httpOnly: true,
    path:'/',
    domain: '.nudepresident.com'
  }
};

const createToken = function(req, res, user, maxAge) {
  // let maxAge = 60 * 60 * 24 * 2;
  var tokenObj = getToken(req, maxAge, user);

  // Set cookie
  res.setCookie(COOKIE_NAME, tokenObj.token, cookieOption(maxAge));

  return tokenObj;
}

module.exports = {

  // Validation
  isValid: function(req, res) {

    if(process.env.NODE_ENV == 'development') {
      return true;
    }
    
    var token = getCurrentToken(req);
    var valid = true;

    // 토큰이 존재하지 않는 경우 그냥 넘김
    // 존재할 경우만 채크하여 유효성 검사
    // 이후 각 endpoint에서 권한별로 처리함
    if (!token) {
      console.log('Token not exist');
      return true;
    }

    // 세션 만료시 PUBLICH 토큰 생성 하고 에러를 보냄
    var now = moment().unix();
    console.log(token, 'TOKEN validation');

    console.log(token.expire);
    console.log(now);

    if (token.expire < now) {
      console.log("Invalid token: session expired" );
      valid = false;
    }

    // if (token.origin != req.headers.origin && req.method != 'POST') {
    //   console.log("Invalid token: domain doesn't match", token.origin, req.headers.origin );
    //   valid = false;
    // }

    if (token.ip != req.clientIp) {
      console.log("Invalid token: ip doesn't match" );
      valid = false;
    }
    
    if (token.ua != req.headers['user-agent']) {
      console.log("Invalid token: user agent doesn't match" );
      valid = false;
    }

    if (!valid) {
      res.clearCookie(COOKIE_NAME, cookieOption(1));
    }

    return valid;
  },

  // Create Session
  create: function(req, res, user, maxAge) {
    // let maxAge = 60 * 60 * 24 * 2;
    return createToken(req, res, user, maxAge);
  },

  extend: function(req, res) {
    var token = getCurrentToken(req);
    var user = token.user;
    if (user) {
      console.log('토큰 갱신', user);
      return createToken(req, res, user);  
    }
    return null;
  },

  // Clear Session
  clear: function(req, res) {

    res.clearCookie(COOKIE_NAME, cookieOption(1));  // Remove this old cookie 
  },

  // Get Session User
  user: function(req) {
    var token = getCurrentToken(req);
    return token.user;
  },

  permission: function(req) {
    var p = getCurrentPermission(req);
    console.log(p);
    return p;
  },

  decode: function(token) {
    return jwt.decode(token, secret);
  }

};
