const fs = require('fs');
const moment = require('moment');
const uuid = require('node-uuid');
const async = require('async');
var sharp = require('sharp');

//AWS
var AWS = require('aws-sdk');
AWS.config.region = 'ap-northeast-2';
AWS.config.loadFromPath(__dirname + '/../s3_config.json');

var FileHelper = function(){};


/* Save Photo
 * callback(error, savedFile)
 * saved file example
  { 
    "url":"http://upload.nudepresident.com/photo/2016/09/10/6c84fb90-12c4-11e1-840d-7b25c5ee775a.jpg",
    "bucket":"upload.nudepresident.com",
    "uri":"photo/2016/09/10/6c84fb90-12c4-11e1-840d-7b25c5ee775a.jpg"
  }
 */
FileHelper.prototype.savePhoto = function(file, cb) {

  if (!file || !file.name) {
    return callback(null, {});
  }

  async.waterfall([
    function thumbnail(callback) {
      createThumbnails(file, function(err, files) {
        callback(err, files);
      });
    },
    function s3(files, callback) {
      sendToS3(files, function(err, result) {
        callback(err, result);
      });
    },
  ], function(error, result) {
    cb(error, result);
  });

};

FileHelper.prototype.saveAttachment = function(file, cb) {
  if (!file || !file.name) {
    return callback(null, {});
  }

  async.waterfall([
    function s3(callback) {
      sendToS3([ file ], function(err, result) {
        callback(err, result);
      });
    },
  ], function(error, result) {
    cb(error, result);
  });
}

function sendToS3(files, cb) {
  var s3_dir = 'photo/' + moment().format('YYYY/MM/DD') + '/';

  var result = {};
  async.eachOf(files, function(file, key, callback) {
    var s3 = new AWS.S3();
    var param = {
      'Bucket':'images.nudepresident.com',
      'Key':s3_dir + file.name,
      'ACL':'public-read',
      'Body': file.stream ? file.stream : fs.createReadStream(file.destPath),
      'ContentType':file.mimetype ? file.mimetype : 'image/png'
    }
    s3.upload(param, function(err, data){
      result[key] = data;
      callback(err, data);
    })
  }, function (err){
    if (!err) {
      cb(null, result);
    } else {
      cb(err, null);
    }
    // delete temporary files
    deleteTmpFiles(files);
  });
}

function deleteFromS3(raw, cb) {
  
}

/*
 * callback(error, filePath)
 */
function createThumbnails(file, cb) {

  // Temporary
  var tmpDir = __dirname + '/../tmp';

  // Ext
  var type = file.type.split('/')[1];
  var ext = file.name.split('.')[1] || type || 'jpg';
  var newName = uuid.v1() + '.' + ext;
  var newPath = tmpDir + '/' + newName;
  var srcPath = file.path;

  var options = ['original', 'w1024', 'w640', 'w320'];
  var files = {};
  for (var i in options) {
    var key = options[i];
    var suffix = '_' + key;
    var name = newName + (key == 'original' ? '' : suffix);
    var destPath = tmpDir + '/' + name;
    files[key] = {
      destPath: destPath,
      name: name
    }
  }

  async.parallel({
    original: function(callback) {
      console.log('original : ', files.original.destPath);
      console.log('src : ', srcPath);
      // Just move file
      var file = fs.createReadStream(srcPath, {flags: 'r'} );
      var out = fs.createWriteStream(files.original.destPath, {flags: 'w'});
      file.on('data', function(data) {
        out.write(data);
      });
      file.on('end', function() {
        console.log('end');
        out.end(function() {
          console.log('Finished writing to file');
          callback(null, 1);
        });
      });
    },
    w1024: function(callback) {
      sharp(srcPath)
        .resize(1024)
        .toFile(files.w1024.destPath, function(err) {
          callback(err, files.w1024.destPath);
        });
    },
    w640: function(callback) {
      sharp(srcPath)
        .resize(640)
        .toFile(files.w640.destPath, function(err) {
          callback(err, files.w640.destPath);
        });
    },
    w320: function(callback) {
      sharp(srcPath)
        .resize(320)
        .toFile(files.w320.destPath, function(err) {
          callback(err, files.w320.destPath);
        });
    }
  }, function(err, result) {

    // delete source file
    fs.unlinkSync(srcPath);
  
    if (err) {
      // delete temporary files
      deleteTmpFiles(files);
      console.error(err);
      cb(err, null);
    } else {
      console.log(result);
      cb(null, files);
    }
  });

}

function deleteTmpFiles(files) {
  for (var i in files) {
    var dest = files[i].destPath;
    if (dest) fs.unlinkSync(dest);
  }
}

module.exports = FileHelper;
