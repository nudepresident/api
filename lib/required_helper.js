module.exports = {
  all: function(required_params, params, callback) {
    for (var i in required_params) {
      var key = required_params[i];
      if (params[key] == undefined) {
        return callback({code: 400, message:'`'+key+'` is required'}, null);
      }
    }
    return callback(null, params);
  },
  oneOf: function(required_params, params, callback) {
    var valid = false;
    for (var i in required_params) {
      var key = required_params[i];
      if (params[key] !== undefined) {
        valid = true;
        break;
      }
    }
    var error = null;
    if (!valid) {
      error = {code: 400, message: "Required one of [" + required_params.join(',') + "] parameter"};
    }

    return callback(error, params);
  },
  oneOfValues: function(required_values, value, callback) {
    if (!required_values || typeof required_values != 'array') 
      return callback({code:'400', message:'Invalid required values'}, null);

    if (required_values.indexOf(value) > -1)
      return callback(null, value);

    return callback({code: 400, message: 'The value has to be one of [' + required_values.join(',') +']'}, null);
  }
};