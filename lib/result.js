const async = require('async');
const ResultModel = require('../models/result');

function getTotalResult(user_id, callback) {
  var resultModel = new ResultModel(this.version);
  async.series({
    answerPoints: function(cb) {
      resultModel.answerPointsSummary({user_id: user_id}, function(error, result){
        if (error)
          cb({code:500,description:"Database Error ",error:error}, null);
        else 
          cb(null, result[0]);
      });
    },
    promisePoints: function(cb) {
      resultModel.promisePointsSummary({user_id: user_id}, function(error, result){
        if (error)
          cb({code:500,description:"Database Error ",error:error}, null);
        else 
          cb(null, result[0]);
      });
    }
  }, function(error, result){
    if (error)
      callback(error, null);
    else {
      var counts = parseInt(result.answerPoints.count) + parseInt(result.promisePoints.count);
      var t = {
        '2': (parseFloat(result.answerPoints.sum_2) + parseFloat(result.promisePoints.sum_2)) / counts,
        '3': (parseFloat(result.answerPoints.sum_3) + parseFloat(result.promisePoints.sum_3)) / counts,
        '5': (parseFloat(result.answerPoints.sum_5) + parseFloat(result.promisePoints.sum_5)) / counts,
        '7': (parseFloat(result.answerPoints.sum_7) + parseFloat(result.promisePoints.sum_7)) / counts,
        '8': (parseFloat(result.answerPoints.sum_8) + parseFloat(result.promisePoints.sum_8)) / counts,
      }
      callback(null, t);
    }
  });
}

function getByCategoryResult(user_id, callback) {
  var resultModel = new ResultModel(this.version);
  async.series({
    answerPoints: function(cb) {
      resultModel.answerPointsSummary({user_id: user_id, group_by_category: true}, function(error, result){
        if (error)
          cb({code:500,description:"Database Error ",error:error}, null);
        else {
          delete result.info;
          cb(null, result);
        }
      });
    },
    promisePoints: function(cb) {
      resultModel.promisePointsSummary({user_id: user_id, group_by_category: true}, function(error, result){
        if (error)
          cb({code:500,description:"Database Error ",error:error}, null);
        else {
          delete result.info;
          cb(null, result);
        }
      });
    }
  }, function(error, result){
    if (error)
      callback(error, null);
    else {


      var r = {};
      var a = result.answerPoints;
      var p = result.promisePoints;
      

      // Sum Points
      for (var i in a) {
        var cId = a[i].category_id;
        var c = r[cId] || {};

        c['2'] = parseFloat(a[i].sum_2);
        c['3'] = parseFloat(a[i].sum_3);
        c['5'] = parseFloat(a[i].sum_5);
        c['7'] = parseFloat(a[i].sum_7);
        c['8'] = parseFloat(a[i].sum_8);
        c['count'] = parseInt(a[i].count);

        r[cId] = c;
      }

      for (var i in p) {
        var cId = p[i].category_id;
        var c = r[cId] || {};

        var v2 = parseFloat(p[i].sum_2),
            v3 = parseFloat(p[i].sum_3),
            v5 = parseFloat(p[i].sum_5),
            v7 = parseFloat(p[i].sum_7),
            v8 = parseFloat(p[i].sum_8),
            count = parseInt(p[i].count);

        c['2'] = c['2'] ? c['2'] + v2 : v2;
        c['3'] = c['3'] ? c['3'] + v3 : v3;
        c['5'] = c['5'] ? c['5'] + v5 : v5;
        c['7'] = c['7'] ? c['7'] + v7 : v7;
        c['8'] = c['8'] ? c['8'] + v8 : v8;
        c['count'] = c['count'] ? c['count'] + count : count;

        r[cId] = c;
      }

      for (var j in r) {
        var x = r[j];
        x['2'] = x['2'] / x['count'];
        x['3'] = x['3'] / x['count'];
        x['5'] = x['5'] / x['count'];
        x['7'] = x['7'] / x['count'];
        x['8'] = x['8'] / x['count'];
        delete x['count'];
      }

      callback(null, r);
    }
  });
}

var Result = function(version) {
  this.version = version;
}

Result.prototype.getTotalResult = getTotalResult;
Result.prototype.getByCategoryResult = getByCategoryResult;



module.exports = Result;
