module.exports = function userMatrixColumn(user) {

  const job = {
    '자영업': 'self',
    '직장인': 'office',
    '전문직': 'specialized',
    '자유직': 'free',
    '사업가': 'entre',
    '가정주부': 'housewife',
    '학생': 'student',
    '퇴직자': 'retired',
    '공무원': 'servant',
    '기타': 'etc'
  };

  const age = {
    '20': 'a20',
    '30': 'a30',
    '40': 'a40',
    '50': 'a50',
    '60': 'a60'
  };

  const gender = {
    'M' : 'male',
    'F' : 'female'
  };

  const married = {
    '0' : 'not_married',
    '1' : 'married'
  };

  const has_children = {
    '0' : 'no_child',
    '1' : 'has_child'
  }

  var columns = [];
  if (user.age)
    columns.push(age[user.age]);

  if (user.gender)
    columns.push(gender[user.gender]);

  if (user.married)
    columns.push(married[user.married]);

  if (user.job)
    columns.push(job[user.job]);

  if (user.has_children)
    columns.push(has_children[user.has_children]);

  return columns;

};