const squel = require('squel');


var sql = squel.select({autoQuoteFieldNames: false})
            .field('t.id','id')
            .field('m.tag', 'name')
            .field('t.category_id')
            .from(`nudep_v3.tmp_recomm_matrix`,'m')
            .left_join(`nudep_v3.tag`,'t','t.name=m.tag')
            .order('weight', false);

console.log(sql.toString());


// const TestModel = require('./models/test');
// var testModel = new TestModel(1);

// // testModel.schemas({}, function(error, result){
// //   console.log(result);
// // });

// var ids = [];
// testModel.userTests({user_id: 94, schemas:['nudep_v1','nudep_v2']}, function(error, result){
//   if (error)
//     console.log({code:500, error: {description: 'Database Error', error: error}}, null);
//   else {
//     delete result.info;
//     console.log(result);
//   }
// });